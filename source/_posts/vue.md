---
layout: post
title: "Vue.js 教學"
subtitle: "淺談元件化很強大的前端框架 Vue.js"
date: 2017-01-27
author: "Lin Yen-Cheng"
catalog: true
tags:
  - JavaScript
---

## Vue.js Intro

了解概念的話，官方的文件真的是簡單易懂到一個好棒棒的境界 XDDD 從頭到尾看過一遍，實際上試著外掛到原有的程式上試玩，很快就可以有初步的了解了!!!

要開始，建議直接使用 [vue-cli][1]，推薦用 webpack 和 webpack-simple 這兩個 templetes，基本的配置檔都配置好了，webpack 這個也加入了[vue-loader][2]，可以完全用元件的概念去寫程式，vue-loader 讓我們能夠可以把所需都裝在一個.vue 檔裡，css 也提供了 scoped 這個強大~~到讓人可以偷懶~~的功能 XDDD

## 元件化

對元件有興趣的話，蠻推薦這篇介紹 [web-components 相關知識的文章][3]，目前 web-components 還只是個潮潮的想法，陸續有越來越多的支持，像是對面當紅 wechat 小程序中的 module，其實就有去實做出來，而.vue 的 component 就相當於 web-component 中定義元件的 template，元件因此成為一個封閉環境，運作不再需要依賴其他外部資源，透過特定的寫法就能夠被 import 進程式中重複使用~ .vue 的實作方式也讓潮潮的想法有了更好的相容性~

和 React 及 Angular 比起來，vue 少了很多的假設，多了很多彈性，也能夠很簡單的做到雙向資料流，當然也可以參考 Redux 的想法把狀態統一管理，像是~~偷懶的~~把資料放在 localStorage 中，這樣就可以統一取得了對吧 XDDD 可是其實還是少了順序性 Orz

## 相關資源

因為 vue 蠻熱門的關係，所以當然有[相當多的資源][4]可以使用，也有用來實作 SPA 路由的 [vue-router][5] 和狀態管理的 [vuex][6]，所以看得出來若是需要多的功能，都是需要自己額外加裝的 XDDD 最重要的是目前大部分主流的編輯器也都有蠻好的支援了~!!!

### 我的相關實作

目前有用過 Vue 做過以下兩個 SPA

1. [Vue js Note][vue js note]
2. [Medicare][medicare]

[1]: https://github.com/vuejs/vue-cli
[2]: https://github.com/vuejs/vue-loader
[3]: http://blog.techbridge.cc/2017/01/06/web-components/
[4]: https://github.com/vuejs/awesome-vue
[5]: https://github.com/vuejs/vue-router
[6]: https://github.com/vuejs/vuex
[medicare]: https://www.health.ntpc.gov.tw/medi
[vue js note]: https://linyencheng.github.io/Note/
