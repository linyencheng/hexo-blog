---
layout: post
title: "開發者工具教學"
subtitle: "從搶票、搶購的按鈕來淺談瀏覽器開發者工具"
date: 2018-12-04
author: "Lin Yen-Cheng"
catalog: true
tags:
  - Tools
---

### 開發者工具簡介

目前主流的網頁設計都是採前、後端分離的概念，所以才有像前端攻城獅這樣的職業出現，那前端和後端的溝通，溝通上一定有一些[安全上需要注意的東西][1]，普遍也會針對這些點去做防範。

今天主要介紹瀏覽器的開發者工具，Chrome 的話可以按 F12 開啟或是 `Ctrl + Shift + I` 也可以從右上角選單的更多工具中開啟。

### Console

Console 的部分也就是程式語言的執行環境，而 Javascript 這個語言是不用編譯的，也就是你直接打 `console.log('hello')` 按下 enter 後就會執行，搶票的時候也可以利用這個做一個簡易的連點功能，等一下會有簡單的示範。

### Element

但常常就會發現，那?按鈕不能按怎麼辦？通常有按鈕又不能按就是被 disable 了，所以可以用開發者工具中的左上角，那個框框包住的小箭頭，按下去我們就可以任意選擇畫面中的任何元素，而輸入框跟按鈕都是可以透過元素的屬性去進行開啟或停用，所以如果想要破解的話，就是看看按鈕有沒有這個屬性??? 有的話恭喜你，對著 disabled 點兩下然後刪除他，然後就發現可以按了。

### 範例

先開記事本貼上下面一段，另存成自己覺得很酷炫的名字，下拉式記得選所有檔案，副檔名用 html 我就是存成 demo.html。

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Button hack</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
  </head>
  <body>
    <button
      id="click"
      disabled
      onclick="(function(){console.log('click!!!'); return false;})();return false;"
    >
      Button I want to click
    </button>
  </body>
</html>
```

接著就可以直接用瀏覽器打開，會看到一個不能按的按鈕，這時候可以按看看，看 console 裡面有沒有出現文字，當然沒有，照著上面說的試一次看看，有就是成功了 xddd 接著又剛好看到傻呼呼按鈕上綁了一個 ID ，這次又恭喜你惹 xddd 這時候只要複製下面這行 `document.getElementById('click').click();` 到 console 執行，這次範例的 id 是 click 所以執行後會發現，這樣就等同於我們手按的效果。可是這樣只有執行一次我想狂點怎麼辦? 也很簡單只要像下面這樣就會每 500ms 幫你自動點一次按鈕惹 xddd 當然如果遇到高竿一點的前端攻城獅，這些可能就無效 QQ

```javascript
let count = 1;
const hack = setInterval(() => {
  count += 1;
  document.getElementById("click").click();
  if (count > 10) clearInterval(hack);
}, 500);
```

### Network

- 狀態欄位：前端正常可以接到 timeout 的訊息，但如果前端攻城獅沒有做例外處理的話，原則上你就會只看到瀏覽器正在載入中，圈圈瘋狂旋轉。打開其中 network 的頁籤，可以看到有一個狀態欄位，我們可以從這裡來做初步的判斷，看到 500 或 timeout 的話，就可以直接再按一次了。
  1. 200 代表沒有問題
  2. 4XX 使用者端
  3. 5XX 伺服器端
- 流量分析，我們也可以透過這裡 Size 觀察到相關的流量變化，在網站載入速度過慢時，這裡會是很好的觀察點。

![network](network.png)

### 小結

其實最容易的，就是明白大概的觀念後，記得多開幾個瀏覽器，先把可以填得先填好，選完第一次沒搶到或沒回應不要灰心，放棄他換到另外一個瀏覽器，等其他人家交易失敗，因為任何因素都可能造成交易沒有成功，切記不要重新整理，可以直接送就再送一次，因為前一次沒成功你剛填的就還有效，就算再重新整理一次，也可能因為爆頻寬而拿不到任何資料。就像是一個正妹很多人 line ，但正妹頻寬已爆，已讀不回、不讀不回 QQ

以下附上動圖支援 xddd

![demo](demo.apng)

[1]: https://linyencheng.github.io/2018/11/15/thinking-security/
