---
layout: post
title: "Hexo 部落格 X Github Page X Google Search Console"
subtitle: "使用 Hexo 架設部落格，發布至 Github Page，讓搜尋引擎檢索的流程說明"
date: 2019-10-15
author: "Lin Yen-Cheng"
catalog: true
tags:
  - Tools
  - Thinking
---

### 伺服器主機

- 確認主機可以從外部進行連結
- 防火牆允許相關服務

### 網域設定

如果有購買網域，則需要讓網域指向我們剛剛設定好的主機，如果是需要使用 Github Page 只需要開好 Repository，舉我的帳號為例，Repository 的名稱要叫做 `linyencheng.github.io`，網站連結就會是:

https://linyencheng.github.io/

- DNS 代管設定 (主網域、子網域)
- [測試工具](https://www.whatsmydns.net/)

### Hexo

接著是去[挑選主題](https://hexo.io/themes/)，這個部落格是使用[別人移植的版本](https://github.com/Kaijun/hexo-theme-huxblog)，以下需進一步配置:

- hexo-deployer-git 發布用
- hexo-generator-feed 產生 RSS 訂閱
- hexo-generator-sitemap 產生網站地圖
- hexo-pwa 支援 pwa
- highlight.js 程式碼上色

### Google Search Console

當確認主機的服務可以被連上以後，我們需要向 google 註冊這個服務，並且提交網站地圖。

網站地圖告訴搜尋引擎我們的網站需要被檢索哪些頁面，在搜尋引擎依據網站地圖建立好索引後，我們的網頁就會出現在搜尋結果中，過一陣子就可以看到相關的成效統計:

- 搜尋曝光數及點擊數
- 錯誤的網址
- 從關鍵字進入

### Google Analytics

若是搭配 google analytics 可以更進一步的看到流量的分析與統計，包含來源還有個頁面的停留時間以及使用者的分析，其中來源是透過 [HTTP Referrer][http referrer] 來進行判斷的，使用的方法一樣是需要先註冊服務，接著在 `_config.yml` 檔中配置即可。

[http referrer]: https://developer.mozilla.org/zh-TW/docs/Web/HTML/Element/a#attr-referrerpolicy

### 網站地圖

檔名一般會是叫 `sitemap.xml`，當我們提交後，搜尋引擎就會透過爬蟲依照網站地圖的資訊來對網站進行檢索，以這個部落格使用的網誌框架 hexo 就有網站地圖產生工具，透過 `hexo-generator-sitemap` 這個工具能讓我們更方便的自動產生網站地圖，接著就是到剛剛註冊的服務中進行提交。

### 爬蟲的設定

除了網站地圖我們還可以設定 robots.txt，這裡主要是告訴搜尋引擎說哪些東西我們不需要被檢索及公開。

```
User-agent: *
Disallow: /cgi-bin/
Disallow: /images/
Disallow: /tmp/
Disallow: /private/
```

### 關鍵字詞規劃

在每一篇文章的標題上盡可能使用容易被搜尋到的字詞組，去增加被搜尋到的可能:

- 關鍵字規劃工具需要用到 Google ADs 的帳號
- 使用 Google Trend
- 搜尋框的自動完成去猜測關鍵字
