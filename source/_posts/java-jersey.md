---
layout: post
title: "利用 Jersey 開發 Java RESTful API 教學"
subtitle: "使用框架來開發 RESTful Web services"
date: 2018-04-06
author: "Lin Yen-Cheng"
catalog: true
tags:
  - Back-End
---

### 常用的 Annotation

基於 JAX-RS 來實現 [RESTful Web Service][restful web service] 的一個 Framework，使用大量的 [Annotation][annotation] 來簡化撰寫，支援 XML(jaxb)及 JSON(jackson)，在 Jersey 中都支援直接將 POJO 轉成這兩種格式的功能，只需要使用@Consumes @Produces 兩種@設定即可。
基本款：

```java
@PATH
@GET, @POST, @PUT, @DELETE
@Consumes
@Produces
@PathParam
// (http://localhost:你開的洞XD/專案名稱/rest/test/我是PathParam)
@FormParam
// 可以用 REST client 製造一個Form 的 POST
@QueryParam
// (http://localhost:你開的洞XD/專案名稱/rest/test?我是QueryParam = 我的值)
@DefaultValue
// 也可以修飾POJO裡的變數
@HeaderParam
@Context
// 進階款：
@Provider
@NameBinding
@Target
@Retention
@ApplicationPath
@XmlRootElement
@BeanParam
@CookieParam
@RolesAllowed
@HeaderParam
```

簡單飯粒~url：http://localhost: 你開的洞 XD/專案名稱/rest/test
rest 就是用 @ApplicationPath 來設置，這樣就透過 Annotation 來幫你分配底層的 Servelt。

```java
@ApplicationPath("/rest")
public class TomcatApplication extends ResourceConfig{
    public TomcatApplication(){
    property(ServerProperties.WADL_FEATURE_DISABLE, "true");
    property(ServerProperties.PROVIDER_PACKAGES, "com.web.tomcat.jersey,com.web.tomcat.filter,com.web.tomcat.interceptor");
    }
}
// test 則是用 @Path 來設置：
@Path("/test")
public class TestRestPractice {
    @GET
    public String hello() {
    return "hello >////<";
    }
}
```

### Filters & Interceptors

Filter can modify any request or response parameters like HTTP headers, URIs and/or HTTP method.
自訂 Server Filters 需要 override 以下兩個方法：
ContainerRequestFilter
ContainerResponseFilter
Interceptors manipulate entities, via manipulating entity input/output streams.  
WriterInterceptor
ReaderInterceptor

加上 `@Provider` 則為預設啟動，若需建立 filter 及 interceptors 與特定 RESTful Services 之間的關連，會利用 `@NameBinding` 再加上自定標註，或是使用 Dynamic Binding。

### NameBinding 飯粒

```java　
@NameBinding
@Retention(value =RetentionPolicy.RUNTIME)
public @interface Test {

}
--------------------------------------------------------
@Provider
@Test
public class TestFilter2 implements ContainerRequestFilter,
 ContainerResponseFilter {
 //
}
--------------------------------------------------------
@Test
@GET
public String hello() {
    return "hello >////<";
}
```

### Dynamic Binding 飯粒　

1. 包在 my.package.admin 裡的

```java
@Provider
public class MyDynamicFeature implements DynamicFeature {
    @Override
    public void configure(ResourceInfo resourceInfo, FeatureContext context) {
    String resourcePackage = resourceInfo.getResourceClass().getPackage().getName();
    Method resourceMethod = resourceInfo.getResourceMethod();
        if ("my.package.admin".equals(resourcePackage)
        && resourceMethod.getAnnotation(GET.class) != null) {
        context.register(LoggingFilter.class);
        }
    }
}
```

2.  HelloWorldResource.class 裡的

```java
public class CompressionDynamicBinding implements DynamicFeature {
    @Override
    public void configure(ResourceInfo resourceInfo, FeatureContext context) {
        if (HelloWorldResource.class.equals(resourceInfo.getResourceClass())
                && resourceInfo.getResourceMethod()
                    .getName().contains("VeryLongString")) {
            context.register(GZIPWriterInterceptor.class);
        }
    }
}
```

如果實作多個，可利用 `@Priority` 來標註優先權~
###@Pre-matching 飯粒

```java
@PreMatching
public class PreMatchingFilter implements ContainerRequestFilter {
    @Override
    public void filter(ContainerRequestContext requestContext)
                        throws IOException {
        // change all PUT methods to POST
        if (requestContext.getMethod().equals("PUT")) {
            requestContext.setMethod("POST");
        }
    }
}
```

提前攔截並更改方法，如未加標註則預設都為 Post-matching 僅做判斷後篩選?

### JAX-RS Response & Error Message

Return Response 可以設定 status

1. .status(Response.Status.OK)//200
2. .status(Response.Status.CREATED)// 201
3. .status(Response.Status.NO_CONTENT)// 204
   - the resource method's return type is void
   - the value of the returned entity is null
4. .seeOther() //303
5. .notModified()//304
6. .temporaryRedirect()//307
7. .status(Response.Status.FORBIDDEN)//403
8. .status(Response.Status.CONFLICT)//409
   取到後可以判斷 Response response = request.get();
   Assert.assertTrue(response.getStatus() == 200);

[restful web service]: https://books.google.com.tw/books?id=zGEYAgAAQBAJ&lpg=PT227&ots=tqA-qA7M1r&dq=ReaderInterceptorContext%20example&pg=PP1#v=onepage&q&f=true
[annotation]: https://openhome.cc/Gossip/Java/Annotation.html
