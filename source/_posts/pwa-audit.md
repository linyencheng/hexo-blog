---
layout: post
title: "用 Chrome Audit 改善效能"
subtitle: "透過 Chrome Lighthouse 稽核並改善網站效能"
date: 2017-11-01
author: "Lin Yen-Cheng"
catalog: true
tags:
  - PWA
---

### Chrome Audit

把 chrome 的開發者工具打開後，Audits 頁籤可以進行幾個項目的測試，測完之後會有一些建議，大概看起來初步要做的就是增加快取的可用性~

### Cache

查了一些快取相關資料後，發現這篇關於[cache][1]的寫得很詳細，另外常用又不太需要更改的函式庫改成使用 CDN 取得，如果有使用 Webpack 的話需要把這些函式庫加入 externals 還有透過 ProvidePlugin 給命名，就可以正常使用，有個小問題就是像是 lodash 這種函式庫建議不要使用 async 的標籤，要不然就是用到什麼 import 什麼，減少一次整包那樣~

```js
externals: {
    jquery: 'jQuery',
    lodash: '_',
  },
plugins:  [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
      _: 'lodash',
    }),
]
```

### Progressive Web App

簡單講一下配置過程，其實只是加入以下：

```html
<link rel="manifest" href="/manifest.json" />
```

然後撰寫相對應的 manifest.json 檔，然後會要跑在 https 底下才可以用，這部分也是未來剛剛說的快取，本來要去 server 拿的資料變成快取在瀏覽器的儲存空間中，透過 service worker 來取得儲存的檔案，我的認知上一樣是為了快取，可以想像成是把 Server 做一個 Client 端的 response cache，這樣就不需要每次都向 Server 要東西~

### 效能 (Performance)

1. SPA 可以加入 server render
2. webpack 的話可以使用 Code-splitting
3. 有使用格線系統像是 bootstrap 可以做 custom build 來減少 css 載入時間
4. 圖片的 lazy load

不過有個問題就是當某些情境下還是會需要高解析度的圖片如何處理?有看到一些方式是後端自動處理圖片成各種解析度，然後透過一定的規則來取用~

### Accessibility & Best Practices

一些好的習慣跟設計上的建議，蠻推薦可以照著建議做看看～

[1]: http://blog.techbridge.cc/2017/06/17/cache-introduction/
[2]: https://developers.google.com/web/fundamentals/performance/speed-tools/
