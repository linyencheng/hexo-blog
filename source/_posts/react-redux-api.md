---
layout: post
title: "加速 Redux 開發 (redux-api)"
subtitle: "使用 redux-api"
date: 2018-08-20
author: "Lin Yen-Cheng"
catalog: true
tags:
  - React.js
  - JavaScript
---

### 為什麼需要 Redux

為什麼需要 Redux? 使用與否的差異在哪?

直接開始一個情境，我們想像一個頁面中有三個元件:

1. 元件一: 登入按鈕區塊，登入後顯示 hello, XXX (XXX 為學生名稱)
2. 元件二: 顯示登入後撈回的各科成績資料
3. 元件三: 更改學生姓名區塊

那麼有或沒有 Redux 的情況下，要怎麼實作這樣的頁面呢？關鍵需要解決的問題就是元件之間溝通的問題:

- 沒有 Redux 時，我們可以使用一個 Container 元件來管理狀態，然後把以上三個元件都當作子元件放在容器裡，並在容器中寫幾個 callback function 當作 props 傳進子元件中，讓子元件可以在改變狀態時,把改變的狀態及時回傳到容器裡，這就是官方文件中寫的 [lift state up][1]

- 有 Redux 最大的改變就是，lift state up to store，把狀態統一管理，這裡有個重要的原則就是 Single source of truth，所有的來源都是來自於一個可被預測的地方，也因為 Redux 這樣的特性，所以 debug tool 可以輕鬆地幫我們做到時空旅行，當操作狀態的函式皆固定時,我們就可以透過操作函式讓狀態停在任意想要的時間點，酷吧 XDDD

[1]: https://facebook.github.io/react/docs/lifting-state-up.html

### redux-api 簡介

在剛接觸 redux 時，會發現被規範的流程，步驟清楚也容易了解，但，卻稍嫌繁複。

1. 狀態都抽到 Store，當需要狀態就需要跟 Store 有連結
2. 控制 Store 只能透過 Action
3. 相關存放邏輯放在 Reducer

為什麼說繁複，當每需要一個共用狀態時，就要寫一個 Action 和一個 Reducer，剛開始還覺得，還拆的蠻簡單清楚的，找東西也很方便，照這樣的理念開工後，只覺得一直在做機械式複製貼上改名稱，開始想想？只是處理個狀態有必要嗎？ Orz

網路上後來有大大，可能?也發現了這樣的問題，於是把這樣的過程封裝起來，實作中也考慮到很多 API 在叫用後端資料時，會需要使用到簡易的 token 認證，於是，簡單的外殼？就這樣出來，現在只需要配置檔跟連接就好了，配置我們的狀態名稱搭配需要打的後端 API，流程大概會是:

1. 叫用 API (可以設定 before 的動作像是 token 檢查等)
2. 成功則用收到的資料呼叫一個 action
3. 到元件中取得 Store 中剛剛 dispatch 的資料

以下是官方範例，將 action, fetch, reducer 結合在一個配置檔中。

```js
import "isomorphic-fetch";
import reduxApi, { transformers } from "redux-api";
import adapterFetch from "redux-api/lib/adapters/fetch";
export default reduxApi({
  // simple endpoint description
  entry: `/api/v1/entry/:id`,
  // complex endpoint description
  regions: {
    url: `/api/v1/regions`,
    // reimplement default `transformers.object`
    transformer: transformers.array,
    // base endpoint options `fetch(url, options)`
    options: {
      headers: {
        Accept: "application/json"
      }
    }
  }
}).use("fetch", adapterFetch(fetch));
```

### redux-api 優缺點比較

可能遇到的問題:
都封裝起來了，錯誤處理怎麼辦？可以用接收 response 的 callback 處理。

- 好處: 當 api 命名不符合習慣的 restful 命名原則，或是 .net 大小寫規則跟大家不一樣，可以映射成順眼的命名，如果是按照 restful 的方式設計，在實作上會非常快速且方便，因為這個外殼已經包含了一個 CRUD 的公版。

- 壞處: 如果遇到更複雜的處理，像是需要套 RX 或是 Saga 時，不太確定要怎麼整合在一起？

參考連結:
https://github.com/agraboso/redux-api-middleware
https://github.com/lexich/redux-api
