---
layout: post
title: "常用 SCSS, CSS 技巧教學"
subtitle: "BEM, 變數使用, mixin, extend, iphone X safe area 避開橫線, hover 換圖片, bootstrap 手機版調整 col 順序"
date: 2018-08-24
author: "Lin Yen-Cheng"
catalog: true
tags:
  - CSS
---

### 命名規則

BEM 命名規則，`header__title--hover`，header 中的 title 在 hover 時的狀態，覺得這樣的命名方法提供了一個統一又易讀的原則，有原則可預測性就高，就算什麼寫法都不去背誦，也可以透過這樣的原則來理解當時的想法。

### 變數使用

使用定義主色調，這樣在未來需要調整主色調時速度會快很多，不用全部搜尋再取代還擔心取代錯誤。
`$primary-color: #ffffff;`
`$second-color: #000000;`

### @mixin @extend

`@mixin` 的做法是可以帶入變數，然後生成客製化樣式，譬如當同樣的一個版但需要套不同圖或是顏色，這樣的做法就會非常方便，`@extend` 就比較單純可以當成注入性質就好了，mixin 搭配 [rwd 的寫法][1]

```scss
@mixin setSpecail($size, $color) {
  font-size: $size;
  color: $color;
}
.IAmSpecailOne {
  @include setSpecail(sizeOne , colorOne);
}
.IAmSpecailTwo {
  @include setSpecail(sizeTwo , colorTwo);
}

％button {
  width：80 px ;
  height：40 px ;
}

.button--primary {
  @extend  ％button ;
  background-color：white ;
}

.button--success {
  @extend  ％button ;
  background-color：green;
}
.button--error {
  @extend  ％button ;
  background-color：red;
}
```

那跟 bootstrap 那種一直加上去的方法差異在哪? `class=”btn btn-default btn-large”`，覺得差異在函式庫要比較彈性，所以需要多寫幾種，但如果是自己要用，像 “button” 這種可能就不用出現了 @@ 直接用 % 告訴編譯器說這個不用了，只是個注入用的變數。

### iphone X safe area

因為 iphone X 把按鈕拔掉了，所以操作介面上，底部會出現一條線，滿版的螢幕上也會有瀏海， 所以蘋果定義了幾個特殊屬性讓大家設定，讓大家可以透過[安全顯示範圍][2]的值來控制上下左右是否需要留白

`padding-bottom: constant(safe-area-inset-bottom);`
`padding-bottom: env(safe-area-inset-bottom);`

### hover 換圖

如果今天有需求是需要在 hover 換圖，直覺上會想要去抽換掉 image 的 url，在 chrome 可以直接在 content 裡面進行設定，但是 firefox, edge 就會有問題，所以又需要多寫一個偽元素並加在偽元素中，這種都是暴力解的方式，其實最好的方法還是不要用 img，改用一個假的容器設定圖片高度再加上 background-image 就好了。

```scss
img {
  content: url("./img/test.png");
  &:hover {
    content: url("./img/test2.png");
  }
}

.mockimg {
  background-image: url("./img/test.png");
  &:hover {
    background-image: url("./img/test2.png");
  }
}
```

### bootstrap 手機版調整 col 順序

有時候就是會想要讓手機板的順序比較賞心悅目，這時候就可以用到內建的 pull 以及 push 來改變電腦版的位置，這樣當手機版 push 或是 pull 的效果消失時，就會是想要的手機版順序了。

```html
<div className="row">
  <div className="col-md-9 col-md-push-3"></div>
  <div className="col-md-3 col-md-pull-9"></div>
</div>
```

### 字型

因為 safari 跟 chrome 的關係，所以會需要有不一樣的字形，兩種都挑了類似的之後，覺得可以就太天真了。作業系統也會影響，會 windows 的 chrome 跟 mac 的字型其實也有點差異，底下是參照網路論壇的，然後拿掉幾個不適用的，為什麼會有不適用的出現，因為考慮到中文字以及需要用的特殊字元，各位大大可以自由調換順序，看看結果如何。

```css
font-family: PingFang TC, 黑體-繁, Heiti TC, 蘋果儷中黑, Apple LiGothic Medium, 微軟正黑體,
  Microsoft JhengHei, sans-serif;
```

### 動畫

蠻推薦 `animate.css` !!!! 也有提供 custom build 的功能。
https://daneden.github.io/animate.css/

### 寬度計算

`box-sizing: border-box;`， width 和 height 屬性包括內容（content），內邊距（padding）和邊框（border）。

[1]: https://blog.hellosanta.com.tw/%E7%B6%B2%E7%AB%99%E8%A8%AD%E8%A8%88/%E5%89%8D%E7%AB%AF/%E4%BD%BF%E7%94%A8sass-mixin%E4%BE%86%E9%96%8B%E7%99%BCresponsive%E7%B6%B2%E7%AB%99
[2]: https://medium.com/@onotakehiko/iphone-x-%E3%81%AE-safari-%E3%81%AB%E3%81%8A%E3%81%91%E3%82%8B-web-%E3%82%B3%E3%83%B3%E3%83%86%E3%83%B3%E3%83%84%E3%81%AE%E8%A1%A8%E7%A4%BA-58da5f503d0b
