---
layout: post
title: "網站安全檢查"
subtitle: "從前端攻城獅的角度簡單看資訊安全問題"
date: 2018-11-15
author: "Lin Yen-Cheng"
catalog: true
tags:
  - Thinking
---

### 輸入網址連線

加密的傳輸，現在 chrome 都會自動幫我們遮蔽沒有加密的連線，也就是沒有 https 的網站就會被警告了，那我們的網站要怎麼擁有 https 的加密傳輸? 最容易的設定就是用 cloudflare 簡單打個勾做好設定，另外快取的機制像是 CDN 以外，還可以擋基本的 DDoS，網頁安全的無腦第一步，先開個免費版吧，伺服器端的話想更安全自己額外加裝的話可以使用 [sslforfree][sslforfree] ，推推啊。

網址觀察的部分，瀏覽器只要網址有遇到 `#` 號，對瀏覽器來說就已經不是網址了，`#` 後面的會放到 scrpit 中去執行，預設的話會找那個 id 的 element 捲動到該位置，也就是說 `#` 號的東西只對瀏覽器有效，當然，這 87%肯定有事件，而且會被執行。舉例來說: `http://localhost:3000/#/test` 我們就可以透過 `window.location.hash` 得到 `/test`，可以使用 `onhashchange` 來監聽並做操作，那如果有人利用這個來做惡意操作騙你怎麼辦? 所以以後看到這種有特殊符號的就不要亂點，可是如果對方很天才縮網址或用 QRcode 呢 xddd 可以明白 goo.gl 為什麼會被 google 停掉了吧 QQ

### 輸入資料

輸入資料的過程中如果要被攻擊，那當然就是會想網頁的輸入框中，如果輸入惡意程式碼進去，這段程式碼會不會被執行? `<script>alert(0.0)</script>` 其實前端如果使用像是 react 這樣優秀的函式庫，在使用 `dangerouslySetInnerHTML` 這類包含危險提示的時候都注意一點，很難踩到坑的，不過上次去 Oath 有被問到伺服器渲染問題，於是發現在土砲伺服器渲染過程中，需要把伺服器做出來的狀態重新給到前端的 store，這個時候這段就很有可能發生問題，那時候被提醒可以做的是就是把 state 字串編碼再解碼。

### API 交換資料

資料傳輸的過程中會不會發生問題，瀏覽器很雞婆都會發 OPTIONS 方法送出請求到另一個網域，去確認是否跨網域以及方法是否可用，也就是我們常常聽到的 CORS 問題，那 API 會不會被機器人一直試? 就是看這一關有沒有擋，擋了之後有沒有可能在 client 端被偽裝，如果不是專業的，要怎麼預防，express 有[相關說明及建議][express]:

- Content-Security-Policy 標頭，以防範跨網站 Scripting 攻擊和其他跨網站注入。
- 移除 X-Powered-By 標頭。
- Public Key Pinning 標頭，來防範使用偽造憑證的中間人攻擊。
- Strict-Transport-Security 標頭，以施行安全的 (HTTP over SSL/TLS) 伺服器連線。
- X-Download-Options（適用於 IE8+）。
- Cache-Control 和 Pragma 標頭，以停用用戶端快取。
- X-Content-Type-Options，以阻止瀏覽器對脫離所宣告內容類型的回應進行 MIME 探查。
- X-Frame-Options 標頭，以提供 clickjacking 保護。
- X-XSS-Protection，以便在最新的 Web 瀏覽器中啟用跨網站 Scripting (XSS) 過濾器。

交換資料再加上鑰匙，也就是伺服器端可以產生伺服器的 cookies 或是短時效性的 token，client 端能做的就是把自己的資訊跟伺服器講，像是產生 [fingerprint][fingerprint]，這樣伺服器至少可以保證每次的請求都是當下這個環境來的，把這兩種訊息做結合判斷，應該可以擋掉部分偽裝的請求。

### 保持登入狀態

cookies 的使用，一來可以辨認身分，二來也可以用來儲存狀態，最後就是因為會跟著請求回到伺服器，所以廣告業也可以用這樣的方式在目標對象按按鈕的時候得到相關資訊，那每次的無痕都會是一個新的環境，fingerprint 會不一樣，當然最快的絕對不是記錄環境，而是直接讓使用者按按鈕，把該打的東西一起傳回伺服器像是 facebook pixel，這樣下次就可以在臉書打到使用者了，最近因為 GDPR 的關係，很多東西變得比較嚴格，原則上只有自己的網域可以使用，那子網域怎麼辦，當然設計上沒有這麼死板，設定一下還是一樣可以使用的，畢竟是同個域名。

如果沒有實作 cookies? sessionStorage 和 localStorage 的組合技也是一個可以保持登入狀態的方式，token 就需要有交換機制，當然最好的也是配合儲存一下使用者的環境，架構如下圖，資料來源(https://auth0.com/blog/refresh-tokens-what-are-they-and-when-to-use-them/)。

<img src="https://cdn.auth0.com/blog/refresh-token/diag2.png">

[sslforfree]: https://www.sslforfree.com/
[express]: https://expressjs.com/zh-tw/advanced/best-practice-security.html
[fingerprint]: https://github.com/Valve/fingerprintjs2
