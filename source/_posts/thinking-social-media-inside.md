---
layout: post
title: "請輸入檢索詞 WWW X 社群"
subtitle: "淺談搜尋引擎與社群媒體對社會的影響"
date: 2019-08-13
author: "Lin Yen-Cheng"
catalog: true
tags:
  - Thinking
---

### 關於社群

> 社群，是人因為目標聚集

首先我們要問幾個問題：

- 對象是誰？
- 喜歡什麼？
- 什麼人、事、物對他們有影響力？
- 何時上網？
- 使用什麼社群媒體？

接著透過像 Social Backer 的統計，從最多人使用的社群工具來觀察當地人在意的是什麼。

- [Facebook](https://www.socialbakers.com/statistics/facebook/pages/total/taiwan/)
- [YouTube](https://www.socialbakers.com/statistics/youtube/channels/taiwan/)

如果目標是年輕人 (me)，也可以從 IG 來著手，像是反送中屬於社會議題，至今兩個月左右已經有了 11 萬的 hash tag，值得好好關注 (香港人口 700 萬)。

- [KOL](https://ninjaoutreach.com/taiwan-influencers/)
- [Hash Tag](https://influencermarketinghub.com/25-most-popular-instagram-hashtags/)

![instagram-hash-tag](instagram-hash-tag.png)

### 地方性的社群

除了全球性的社群軟體，地方性的社群也很重要，舉例來說像是台灣的批踢踢、爆料公社、香港的連登，現在也成為媒體的資訊來源，以這次香港反送中來說，在媒體無法信任的情況下，民眾就能夠透過連登進行更快的訊息交流。

![lihkg](lihkg.png)

此外也透過社群的推廣，在短短三小時時募集了 190 萬美金，這個故事告訴我們，真的要好好努力賺錢，不對，是不可以忽視網路的力量。

> 在網路上沒有被遺忘的權力 (請輸入檢索詞 WWW)

- https://crowdwatch.tw/projects/2139
- https://www.gofundme.com/f/standwithhk-international

![gofundme](gofundme.png)

### 請輸入檢索詞 WWW

搜尋引擎的操作，如果有 google ads 的帳號，我們可以使用關鍵字規劃工具、搜尋字詞報表，若沒有也可以使用 google 搜尋趨勢觀察熱搜，用詞上也可以透過自動完成的提示來猜測。

但在看了請輸入檢索詞 WWW 後，可能也要好好思考地方性的搜尋引擎在人工干預上的影響力。

> 「狂牛症」、「彈劾」成為熱門檢索詞不到一分鐘，NAVER 疑似馬上刪去相關資訊

- https://talk.ltn.com.tw/article/breakingnews/2878723
- https://trends.google.com.tw/trends/

![google-trend](google-trend.png)

### 數位外交行動計劃

前陣子去聽了數位外交行動計畫的分享，就是一個社群媒體在外交上應用成功的案例。

這個組織在找到可以切入的問題點後，就從社群開始進入並逐漸擴大影響力，是個厲害的組織。

- https://www.facebook.com/TaiwanDigitalDiplomacy

當然透過社群媒體的力量，也讓我們看見公民素養與影響力的展現，反之也看到運用社群軟體來分化社會的例子 QQ

- https://www.facebook.com/watch/?v=438062300381738
- https://www.facebook.com/newsalaryman689/photos/a.1694563840813756/2339524546317679/

### Sing Hallelujah To The Lord

當警察和政府無法保護人民、無法讓人民正常生活的時候，除了祈禱以外，社群絕對扮演著不可缺少的角色。

> Sing Hallelujah To The Lord 是因為宗教集會在香港是不用申請的

最後，想說現在都 2019 年了，真的值得想想為什要讓民眾對濾毒罐的型號這麼熟悉。

> To take care of life, not to control.

<iframe width="560" height="315" src="https://www.youtube.com/embed/MmBt18knMTQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
