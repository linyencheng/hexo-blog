---
layout: post
title: "AWS lambda vs ec2"
subtitle: "AWS lambda vs ec2 網站管理"
date: 2018-06-04
author: "Lin Yen-Cheng"
catalog: true
tags:
  - Tools
---

### Lambda

對一個大部分時間都在看前端的攻城獅來說 serverless 聽起來是一個蠻夢幻的解決方式，而 AWS lambda 就是這樣的一個解決方案，並不需要過多的伺服器設定馬上就可以讓網站上線了，初步只是想做嘗試的話，網路上已經有人寫了一個[小工具 - scandium tool](https://github.com/LinusU/scandium)，當然還是需要先使用 [aws command line tool](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html) 先初始化 AWS 相關設定。

> 好處就是不用顧機台，而且不用擔心會掛掉。

### Scandium tool

這個工具在發布時會讀取相關配置進行發佈，一些可能的問題大概是靜態檔案要自行放至其他地方，lambda 本身好像並不能讓你放靜態檔案? 初步用起來感覺比較適合單純的後端頂多加簡單的樣板像 ejs 配合使用，然後靜態檔案都放到 S3 去這樣~ 如果是比較複雜的 SPA 還有拆分 chunk 外加一些建置過的樣式檔，還有相關靜態 resoure 因為還要設路徑等等，發佈就變很麻煩... 如果有自己寫好一個 S3 資源管理，圖片就都可以用固定聯結，可能較不會有資源檔的問題。

### ec2

就是一部虛擬機器，好處是不需要重新學習太多 AWS 的相關知識，只要注意好開 port 保存好存取的 key 原則上並沒有太多問題，如果需要管理 domain 我的認知上在 lambda 是沒辦法做 proxy 的，就會需要其他額外的服務，那 ec2 上只需要架個 nginx 就能輕鬆解決，nginx 也可以解決大家常會提到 c10k 的問題(lambda 應該是連有都不會有 xddd)。

接著是[linux 系統上的網站管理(nodejs)](https://linyencheng.github.io/2018/10/04/tool-linux-server/)，可以參考這篇文章。

### 發佈

EC2 上可以架設 jenkins 或著利用其他環境上的 [jenkins 透過 ssh 去發佈](https://linyencheng.github.io/2018/05/01/tool-jenkins/)。
