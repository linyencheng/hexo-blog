---
layout: post
title: "Java 相關筆記"
subtitle: "相關名詞及概念理解"
date: 2018-04-05
author: "Lin Yen-Cheng"
catalog: true
tags:
  - Back-End
---

### 資料物件名詞 (取名主要用來辨識關係)

1. POJO (pure old java object)

```java
public class Employee {
    private int id;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
}
```

2. DAO (data access objects)
   主要配合資料庫
3. PO (persistant object)
   Hibernate example: 當 POJO 被實體化(instance) 有 set 值，接著進入 session 後。
4. DTO（data transfer object）
   POJO 永久化成 PO，PO 組合成 DTO。
5. JavaBean
   POJO 可序列化，有一個無參數的建構子，使用 getter 和 setter 方法來訪問屬性。
6. XML，JSON
   都是 Web 常用的傳輸格式。

現況：表中有 100 個欄位，則相對應的 PO 有 100 個屬性。
情境：界面上只需顯示 10 個欄位。
實作：Client 向服務端取值，服務端不需傳全部的 PO，此時將只有 10 個屬性及傳遞的參數組成 DTO 來傳遞結果。

Hibernate
操作物件就可以完成讀寫資料庫，是一個結合 JDBC 和物件的 Framework。

### @標註

主要就是對程式碼加些@標註，給人看也給機器讀。之前看過的 @Override 就是內建的一個標註，就是要進行 parent 函數覆寫的意思，@Deprecated 被標注的方法或類型已不再推薦使用。

用在其他 Annotation 上的 Annotation

1. `@Retention` 確定標註的生命周期, 用一個 Enum 的 RetentionPolicy 參數設定。
2. `@Documented` 文檔化。
3. `@Target` 表示標註適用的範圍，也用 Enum 的 EnumType 的參數設定。
4. `@SessionScope`?
5. `@Inject`
   針對資料的標註，@JsonProperty 改變 JSON 名稱

```java
public class Name {
  @JsonProperty("firstName")
  public String _first_name;
}
```

自訂標註則需要用到 `@interface`，不同於 interface，這次使用的是@interface

```java
public @interface Test {

}
```

按照上面這樣撰寫後，其他地方就可以使用 @Test 做為自定標註了。

NDI （Java Naming and Directory Interface ），看了一陣子原文介紹之後，真心覺得歪果人發明一堆奇怪的名詞的目的在於！！！讓人覺得自己很蠢，導致會覺得作者很猛 XDDD 然後留一個小難跨過的 Gap，來讓知識不普及 XDDD

### JNDI

譬如當我們想搞一個後端資料連結用的服務，那顯然需要把他配置到你的 Server 上，同時也會需要一個 JNDI name ，接著這個 JNDI Name 跟 interface 就都會被註冊到 JNDI Service。

當 Client 程式在執行時，要先去找 (lookup()) 到一個後端資料連結的 interface ，如果小幸運找到在 JNDI Service 裡面，這時候 JNDI Service 會回給你一個 remote 的 reference ，取得的這個 remote reference 是一個 Object ，有了這個 Object ，你就可以用它做你想做的事情惹譬如去 得到一個後端連結(getConnection)...

是不是覺得以上就算附圖還是說的超級不詳細 XD 是不是看了覺得自己很蠢 XD 是不是覺得他奶奶這樣的知識怎麼能快速普及，是不是可以普及才有鬼 XD

所以簡單來說，雖然是你做你的我做我的，不過出一張嘴的才是算數的，反正最後給我弄出這樣那樣的 OOO 讓我可以用就好，然後打雜小弟就可以開始努力實做出各種來提供，後人也就不用了解像是帳號密碼 IP 位置或是 Driver 去哪下載惹。

突然想起網路上一句話說的很好：一流的企業賣標準，二流的企業賣品牌，三流的企業賣技術，四流的企業賣產品，五流的企業賣勞力，大家共勉之 XD
