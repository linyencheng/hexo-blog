---
layout: post
title: "React Create Script 2.0 簡介"
subtitle: "使用 React Create Script 2.0 減少維護環境配置"
date: 2019-02-27
author: "Lin Yen-Cheng"
catalog: true
tags:
  - React.js
  - Thinking
---

### React Create Script 2.0

主要設計給入門者以及不想維護環境配置檔的開發者使用，如果沒有較偏門的需求，強烈建議採用。

### SCSS

無痛支援必須推，不像之前 1.X 版本還需要裝個外掛 node-sass-chokidar，預先把 SCSS 動態編譯成 CSS 才可以引入。

現在就只需要 `npm install node-sass --save-dev` 後就可以直接 `import './main.scss'` 了。

### 靜態網站發佈

如果只是單純的前端攻城獅，在不需要架後端的前提下，要怎麼讓網頁可以 demo 呢? 最方便的首推 Github Page deploy，Github 提供免費的環境讓大家放靜態網站，寫完網頁之後可以直接無痛發佈到 Github 上 Demo，我們預設的網址會是"你的帳號.github.io"，這個網址專案名需要特殊命名，像我的這個部落格所在專案就會是 linyencheng.github.io。

若是其他的專案會需要多開一個 branch 叫做 gh-pages 一樣把靜態頁面發佈到這裡，而這些動作在配置好後，都可以透過一行指令完成了。

https://facebook.github.io/create-react-app/docs/deployment#github-pages-https-pagesgithubcom

### 環境變數

環境變數的用途就是當我們的建置環境不同的時候，我們會需要做個別的設定，因為測試網站跟正式網站打的後端 API 會不一樣，或著是測試網站需要在網頁的 console 中也有 log，這就需要透過不同的配置來做，若是沒有環境檔又需要持續整合及發佈，這個部分肯定會遇到一些要克服的問題，目前幾次工作下來，發現有兩種設定方式。

- .env 環境檔，可以按照文件自行修訂，如果需要其他自訂的變數，可以按照命名規則新增至環境檔中，將變數都以 REACT*APP* 開頭命名，這樣就能在程式碼中使 process.env.REACT_APP_XXXX 來存取我們寫在配置檔裡面的值，這樣有什麼好處呢?

https://facebook.github.io/create-react-app/docs/adding-custom-environment-variables#adding-development-environment-variables-in-env

如果今天不需要 .map 檔我們該怎麼做，.map 是用來 debug 用的，所以並沒有經過醜化，所以今天如果不需要，我們就可以透過在專案的根目錄新增一個環境變數檔 `GENERATE_SOURCEMAP = true`，讓建置的時候不產生這個檔案。或是同樣一個程式需要 Deploy 到測試及正式環境中，有差異的配置部分就可以透過環境變數來進行設定。

最後我們就在建置的環境先放上環境檔，這樣一來在建置的時候就可以建置出不同需求的檔案來讓大家進行發佈。

- 從雲端主機服務來設定
  Azure 其實這方面就弄的比較適合新手一些，可以透過 App service 裡面的設定來讀取，這個部分是比較適合新手一點的地方。

https://docs.microsoft.com/en-us/azure/app-service/web-sites-configure

### Bundle analysis

react create script 裡頭有這樣的一個配置，可以讓我們分析 Bundle 的 Chunk 大小，當無腦開發到一段時間以後，會發現我們的 bundle 越來越大包，當然初步還是可以使用 react 16.6.0 之後推出的功能來把 bundle 拆成小的 chunk，如果小的 chunk 大小還是太大，就需要使用分析的工具了，這樣的工具可以告訴我們到底哪個函式庫占了比較大的空間。

如果發現用了一個肥大函式庫，但只用了不到 1% 的功能，那就是時候找替代方案或是考慮自己實做了。

https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size#docsNav

### Progressive Web App

這是一個當時我放棄寫 App 的原因，因為發現瀏覽器的進化速度很快，google 也有團隊在持續加強，也相信這是未來的發展，所以就跳坑了，Progressive 漸進式的增強，即便不支援也會自己降級，我認為網頁的唯一缺點在各瀏覽器核心和支援差異。不過在微軟宣布要開始用 Chromium 去進行開發後，我想這個方向就更正確了。

https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### https

PWA 需要在 https 的環境下才可以測試，所以這裡也提供了兩個在開發時候使用 https 的方法

- 軟體
  https://ngrok.com/
- 指令
  https://facebook.github.io/create-react-app/docs/using-https-in-development
