---
layout: post
title: "Jenkins 教學"
subtitle: "流程自動化工具"
date: 2018-05-01
author: "Lin Yen-Cheng"
catalog: true
tags:
  - Tools
---

### Jenkins

簡單介紹一下流程自動化的好幫手，使用前本來有點擔心很難使用 Orz 但使用後發現其實蠻直觀的，針對需求做設定即可,如果說流程是這樣 commit 專案 -> 專案建置 -> ftp 發佈，那我們會需要抓到 commit 的時間，還有建置的環境以及 ftp，所以當然就找到相關的外掛先下載跟看文件。

另外 Jenkins 不執行任務的情況下會使用 200M 左右的記憶體，若構建時會用更多，所以建議伺服器的記憶體至少有 1G，記憶體較小的主機也建議執行的配置上可以加上限制 `-Xmx512`。

1. [下載後安裝](https://jenkins.io/download/)
2. 安裝可能會需要的 plugin
   - [NodeJS Plugin](http://wiki.jenkins-ci.org/display/JENKINS/NodeJS+Plugin)
   - [Publish Over FTP](http://wiki.jenkins-ci.org/display/JENKINS/Publish+Over+FTP+Plugin)
3. 針對 plugin 做相關設置
   - 系統設定 ftp server 帳密
   - 系統設定版控帳密
   - 工具設定 node js 環境

### 自動化流程

接著是把流程自動化 commit 專案 -> 專案建置 -> ftp 發佈

1. 開始一個 free style 的專案，接著暴力的針對原始碼定期自動掃描
   - `Poll SCM H/10 * * * *` (十分鐘一次)
2. 設定 Build 的動作
   - 選擇 windows batch commend
   - npm install
   - npm run build
3. 設定 Post-build Actions
   - 將相關的建置後資料透過 ftp 發佈至伺服器

早該開始用了 Orz 推薦大家！

### 推薦外掛

1. [Startup Trigger][startup trigger]：Jenkins 重啟後會自動重新建置
2. Workspace Cleanup Plugin: 節省空間

[startup trigger]: https://wiki.jenkins.io/display/JENKINS/Startup+Trigger
