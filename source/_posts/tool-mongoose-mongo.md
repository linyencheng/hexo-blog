---
layout: post
title: "Mongoose X Mongo DB"
subtitle: "使用 Mongoose 操作 Mongo DB"
date: 2019-09-07
author: "Lin Yen-Cheng"
catalog: true
tags:
  - Tools
---

### 背景

在 Node.js 出現後，使用 JavaScript 的撰寫越來越方便，編輯器的功能也日漸強大，當然最重要的就是後端也能夠透過 JavaScript 來撰寫。

### 問題定義

當我們要操作資料庫時，透過 OR mapping 的工具會是最方便的，可以減少我們研究資料庫的時間，那後端需要解決的問題就會在如何設定資料庫以及如何操作資料庫。

### 資料庫設定

最常用的就是運用 Mongoose 搭配 Mongo DB:

1. 在安裝完 Mongo 後，進入 shell 去連線看看 `mongo --port 27017`
2. 成功連線後可以選擇資料庫開始下指令，像是 `use test`
3. 進入 test 中設定使用者及權限:

```
db.dropAllUsers()
db.createUser({
    user: "test",
    pwd: "test",
    roles: [{ role: "readWrite", db: "test" }]
})
```

4. 資料庫為了安全起見會要將資料庫的 authorization 打開，windos 請[參考連結](https://blog.yowko.com/mongodb-enable-auth/)，ubuntu 請去編輯 etc 資料夾底下的芒果設定，加入設定如下：

```
security:
   authorization: enabled
```

### 操作方式

Mongoose 則是協助我們在 node.js 專案中操作 Mongo DB，在程式中定義好 schema (不需要開欄位)，確認資料庫連線沒有問題就可以直接開始操作了。

比較大的問題是 node.js 不像 C#.net 那樣的語言有工具可以自動產生程式碼和架構，所以這個部分就是看每個人的狀況去放置，但還是以關注點分離的概念為主:

- 在 model 中定義 Schema
- 在 controller 中撈資料

```js
/**
 * user model
 */
const mongoose = require("mongoose");

const userSchema = mongoose.Schema({
  id: String
});

module.exports = mongoose.model("user", userSchema);
```

```js
/**
 * user controller
 */

const getUser = id => {
  return new Promise((resolve, reject) => {
    const User = require("./model");
    User.findOne({ id }).exec((error, docs) => {
      resolve(docs);
    });
  });
};
```

### Schema 設計

因為不像傳統資料庫那樣有 join 的概念，所以拿資料的時候可以依照我們的使用情境去設計，像是以撈取為主的話就可以透過雙向參照的設計去加速，詳細可以參考[MongoDB Schema 設計指南][two-way referencing]，分類有以下幾種:

- Modeling One-to-Few 少量級關聯模式 (Embedding)
- One-to-Many 多量級關聯模式 (Child-Referencing)
- One-to-Squillions 海量級關聯模式 (Parent-Referencing)
- Two-Way Referencing (雙向參照設計)
- 多對一反正規化 (Denormalizing from Many -> One)

### 專案架構

在網路上有看到常見的兩種架構:

```bash
├── src/
│  ├── entities/                # 按照資料表去區分
│  │  └── user/                 # 使用者表
│  │     ├── model.js           # Schema 定義
│  │     ├── controller.js      # 資料庫 CRUD
│  │     └── route.js           # API 設定
│  └── index.js                 # 入口
├── .eslintrc                   # ESLint 設定檔
├── package-lock.json
├── package.json
└── README.md
```

```bash
├── src/
│  ├── controller/              # 資料庫 CRUD
│  ├── routes/                  # API 設定
│  ├── models/                  # Schema 定義
│  └── index.js                 # 程式入口
├── .eslintrc                   # ESLint 設定檔
├── package-lock.json
├── package.json
└── README.md
```

[two-way referencing]: https://blog.toright.com/posts/4537/mongodb-schema-設計指南-part-ii-反正規化的威力.html
