---
layout: post
title: "JSDoc 教學"
subtitle: "快速上手專案文件產生工具"
date: 2017-01-27
author: "Lin Yen-Cheng"
catalog: true
tags:
  - JavaScript
  - Tools
---

## 安裝

來介紹一下一個厲害又方便~~攻城獅偷懶~~的 module **JSDoc**，可以在短短幾秒內就自動生成出網頁版的文件，超猛 der~

只要下指令安裝 `npm install jsdoc`，然後就可以直接 `jsdoc yourJavaScriptFile.js` 使用了 Orz 不過檔案內容當然要搭配上特殊的註解 `/**` 開頭，這樣才可以被 jsdoc 辨識出來，下面是官網的範例:

```js
/**
 * Represents a book.
 * @constructor
 * @param {string} title - The title of the book.
 * @param {string} author - The author of the book.
 */
function Book(title, author) {}
```

## 配置檔

這麼普及的東西，想當然爾可以整合進 gulp 中，只要記得先裝上 gulp-jsdoc3 XD 不喜歡預設值的話，可以加上自定義的簡單配置檔:

```json
{
  "tags": {
    "allowUnknownTags": true,
    "dictionaries": ["jsdoc", "closure"]
  },
  "source": {
    "includePattern": ".+\\.js(doc|x)?$",
    "excludePattern": "(^|\\/|\\\\)_"
  },
  "plugins": [],
  "templates": {
    "cleverLinks": false,
    "monospaceLinks": false,
    "includeDate": false
  },
  "opts": {
    "encoding": "utf8",
    "recurse": true,
    "destination": "./doc/"
  }
}
```

## 整合進 gulp

接著在 gulp-jsdoc3 中配置套用後，只要告訴 jsdoc 需要幫哪些檔案做文件:

```js
var jsdoc = require("gulp-jsdoc3");

gulp.task("doc", function(cb) {
  var config = require("./conf.json");
  gulp
    .src(["README.md", "./js/**/*.js"], { read: false })
    .pipe(jsdoc(config, cb));
});
```

**就可以早早收工洗洗睡了 Orz**
