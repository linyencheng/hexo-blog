---
layout: post
title: "Avada X WordPress 速度優化"
subtitle: "從 Avada 的相關設定及資源檔快取來優化網站速度"
date: 2019-07-24
author: "Lin Yen-Cheng"
catalog: true
tags:
  - Tools
---

### Avada 優化

相信很多人在入門時，很可能是在看了一些部落客的文章後，在毫無概念的情況下選擇了主題 Avada，Avada 並不是不好，只是蠻容易讓新手遇到問題，遇到問題之後可能就會開始求助於社群，社群裡當然就會遇到很多那些年一起踩過坑的大家。

我是一個前端工程師，所以直接從網站的效能面來看，因為 Avada 是一個相對完整的主題，這也代表著初學者的我們，很可能只是用大砲打小鳥，加上剛開始又使用較一般的機台，跑著一大包完整的功能，自然網站速度也就慢了。

以下從功能面和快取設定來簡單加速網站，部分結果可以搭配[開發者工具][developer]進行使用，[測試時務必使用無痕或是清除快取後測試][reload]。

### Fushion Builder 優化

完整的主題中包含了很多的功能，在啟用主題底下相關功能的時候，我們同時也需要在使用時下載相關的資源檔，這意味者我們開啟的功能越多，需要下載的資源檔越多，這時候速度也就自然被拖慢了。

> 怕你買到被閹割的，預設值全功能開啟

那這些功能性的資源檔在網站中主要會分為兩種：

- 腳本 (.js) ，腳本語言包含相關邏輯
- 樣式檔 (.css)，樣式檔則是顯示的樣子

也就是說通常你打一個勾通常就會多一組資源檔，Fushion Builder 的設定中，未使用到的 Element 建議就直接停用，旁邊的文字說明告知能夠提升效能，若是停用會對網站效能有幫助。另外 Avada 主題選項中，有提供 Performance 選項做相關調整及優化，主要可調整的項目：

- Litebox 功能若未使用可以停用
- google font 若需要使用請改用 CDN 的選項
- JS 的編譯
- 圖片的懶載入

### WordPress 快取

網站在渲染頁面時，一般會分成下面三種情境：

- 伺服器端動態渲染：資料庫的存取 -> 存取後的運算 -> 最終動態渲染的產生
- 伺服器端靜態檔案：Prerender 成一個靜態檔案
- 瀏覽器端渲染：只取網站中骨架，部分資料等網站載入後才透過瀏覽器去取得及抽換

WordPress 會是屬於第一種，在頁面 render (渲染)的過程中，會需要好幾道手續，所以在還沒裝外掛之前你只要拿個壓力測試工具打一下，很容易就可以看到 cpu 99.99% 了 Orz 為了加速這樣的過程我們就需要安裝快取的外掛 (ex: WP Super Cache, [WP Fastest Cache][wp fastest cache])，進行這幾道手續的優化，這個目的主要是將動態渲染的部分進行加速。

### 圖片資源使用 CDN

[CDN (Content Distribution Network)][cdn] 中文全名是內容傳遞網路，簡單來說就是透過這樣的服務我們可以加速資源檔的載入速度，圖片過多又沒有進行相關的處理當然直接拖慢網站載入速度，前陣子跟網友討論，[jetpack][jetpack] 提供將圖片使用免費 CDN 的功能，另外 tinypng 也協助我們將圖片進行不失真的壓縮。

### 系統方面

- PHP 版本及相關設定，可以從以下看到：
  1. Avada 裡面 System Status 2.工具裡頭的 Site Health
- [Litespeed][litespeed] 伺服器，市占率較低，但[官網說有較好的效能][compare-litespeed-apache-nginx]

[cdn]: https://zh.wikipedia.org/wiki/CDN
[jetpack]: https://jetpack.com/support/site-accelerator/
[developer]: https://linyencheng.github.io/2018/12/04/tool-google-developer/
[reload]: https://linyencheng.github.io/2019/06/28/thinking-debug/#清除網頁快取
[litespeed]: https://www.litespeedtech.com/
[wp fastest cache]: https://wordpress.org/plugins/wp-fastest-cache/
[compare-litespeed-apache-nginx]: https://www.litespeedtech.com/products/litespeed-web-server/compare-litespeed-apache-nginx
