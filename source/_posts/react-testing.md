---
layout: post
title: "React.js X 測試"
subtitle: "前端測試踩坑入門"
date: 2017-06-21
author: "Lin Yen-Cheng"
catalog: true
tags:
  - JavaScript
  - React.js
---

### 前端測試

當前端開發元件化以後，要測試的話最基本的就是針對元件做單元測試，依照使用的框架或函式庫不同會有不同的測試工具，像 react 的話 facebook 官方就有 [Jest][jest]，對於單元測試來說非常方便而且也包含了覆蓋率的計算，最後會直接出一個報表給你，文章中也有推薦 enzyme，是 airbnb 開發的工具，據 react 官方說法是讓測試更簡單?!

### 官方文件

https://create-react-app.dev/docs/running-tests

1. 設定環境
2. 找相關關鍵字 Jest、enzyme
3. 理解相關指令像 `npm test -- --coverage`

### 相關問題

第一次寫測試的話會遇到的問題像是，Jest 的腳色是什麼? 為何需要 enzyme? 啥是 shallow & mount ?? 到底要測哪些東西?? 寫法呢??

Jest 是主要的測試工具，只要該放的位置該發樓的命名規則都做好，指令打下去就會跑測試了，enzyme 只是讓我們更方便測試 react 用的外掛，主要是因為 react 有用到 virtual dom ，那測試又會需要去 render，所以 enzyme 就封裝了 [react 原生的測試][react-test]讓寫法更直覺？？

寫法上會用到以下三個基本關鍵字 describe , it , expect，這是撰寫單元測試的語法架構，describe 主要是拿來整理 it 用(在這邊又可以用 test 代替），it 是最小的測試單位，所以每一項測試都要寫在某個 it 裡。it 第一個參數是測試名稱，其實就是給你註解到底這個測試要幹嘛 XD 相關的 it 就可以包在 describe 裡面，同樣參數就是拿來寫說明，也可以再包一層 describe 作更進一步的分類整理。然後搭配 before、beforeEach、after、afterEach 做進階操作~

expect 按照名字來看就是你預期他到底怎樣，搭配 it 這個測試項目，看跑出來的結果是不是跟 expect 中的一樣，這就是基本的測試了!!!

### enzyme

元件測試，從關鍵字去找相關教學
jquery: element -> \$(element) 可以使用 function
enzyme: component -> enzymeComponet 可以使用 function

- shallow: 只 render 第一層
- mount: full render，包含元件週期
- render
- simulate: 模擬 event
- setProps: 設定 props
- setState: 設定 state
- prop(key): retrieves prop value corresponding to the provided key
- state(key): retrieves state corresponding to the provided key

### 細節

- 元件測試，import 後用 mount 然後 props 用假資料進行測試，模擬點擊 `jest.fn()`
- function 測試，import 後用假資料測試
- 非同步: `jest.fn()`
- 包含測試

### Redux 測試

Container 在 mount 時需要加入 mock store 以及有使用到的 middleware

```js
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
const middlewares = [thunk];
const mockStore = configureStore(middlewares);
```

### 相關 API

其他，用真的網頁來測的話，可以用 nightmare 來做，不過測試工具中 jasmine 的 timeout 記得加長一點 XD

[react-test]: https://facebook.github.io/react/docs/test-utils.html
[jest]: http://facebook.github.io/jest/
