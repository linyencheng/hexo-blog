---
layout: post
title: "React 元件的狀態與性質"
subtitle: "從科學角度來看元件設計與變化"
date: 2017-05-18
author: "Lin Yen-Cheng"
catalog: true
tags:
  - JavaScript
  - React.js
---

### 元件

最近比較認真看 [React component][1]相關的東西，發現了一些跟科學上的小關聯，來~讓我們看下去 XD

React 的每個元件都有兩種 Instance Properties，分別是 props 和 state，其中 props 是從其他元件傳過來的不可改變，state 則是可以透過運算或條件來改變的~

### 熱力學

看到這些不小心就回想起剪旁邊的名言，什麼是 Property (黑人？？) 性質??!!? 老師給你０分 XDDD

Property 在我的理解上會是在一個系統中且可量測的某種狀態，系統簡單來看可以當作是被限制住的空間也就是 control volume，若是在系統中，狀態能成為 homogeneous 的 steady state，也就是達到巨觀可量測的條件，那麼那個狀態我們就叫做 Property，簡單來說就像是 20 度時氣球的內部壓力?!!!

### 從科學看元件

回到 react 元件中，狀態都會因為函式或是程式邏輯去改變，由於程式是一行行跑的，在元件中操作元件的最後一行後，狀態也就不會再變化，這時候的狀態就可以看成是 Property，而不變的性質，就會以外顯的方式顯示在使用者介面上了，那一個元件中運算出的 state 也就可以當成 properties 傳遞給所包覆的小元件當作 props 來使用。想想之後發現根本 87% 像，對吧 XDDD

而狀態與性質搭配元件的週期就會引發元件的改變，如圖。由於程式是一行行跑的，在元件中操作元件的最後一行後，狀態也就不會再變化也就是 Mounting 這個部分。唯有透過事件觸發取改動狀態或是外設性質的改變才會驅動畫面的渲染。主要常用的也就是 componentDidMount (穩定不在變化後)、componentDidUpdate (被狀態或外設性質觸發改變) 這兩個週期。

更詳細的說明，可以到圖片來源的連結去看看。 (http://projects.wojtekmaj.pl/react-lifecycle-methods-diagram)

![state](/images/state.png)

[1]: https://facebook.github.io/react/docs/react-component.html#props
