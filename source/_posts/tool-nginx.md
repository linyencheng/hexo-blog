---
layout: post
title: "Nginx 設定與效能優化教學 (Ubuntu, SSL, reverse proxy)"
subtitle: "在 Ubuntu 上利用 Nginx 架設網站伺服器 (Ubuntu, SSL, reverse proxy)"
date: 2019-07-13
author: "Lin Yen-Cheng"
catalog: true
tags:
  - Tools
---

### Nginx 簡介

Nginx 是一個非同步框架的網頁伺服器，也可以用作反向代理、負載平衡器和 HTTP 快取，在 Non Blocking IO 的機制上，可以提供很好的響應速度，解決了伺服器 C10K 的問題。

### Non Blocking IO

Nginx 在 Linux 的 IO 機制上有支援 epoll，所以在靜態網頁的效能上遠超過使用 select 機制的 apache，而且只需要透過簡單配置即可使用，還有其他支援的機制可以參考[官方文件][1]。

```
events {
	use epoll;
	worker_connections 1024;
	multi_accept on;
}
```

### SSL 安全性連線

另外主要的用途可用來掛 https，在 https 安裝上推薦使用憑證機器人

```
sudo certbot --nginx -d test.domain.com
sudo certbot delete --cert-name test.domain.com
sudo certbot renew --dry-run
```

跑過以後可以在檔案裡面可以看到 # managed by Certbot 的註解，代表這份配置檔的部分設置就不需要我們維護了，[Digital Ocean 提供了很詳盡的教學][2]，非常推薦。

### 基礎設定

server_name 就是搭配我們的網域，接下來只要在 DNS 指向主機就大功告成。靜態的網站就較為單純，指向正確就可以了，但若是像 php 屬於動態網頁就還需要搭配 fastcgi 做相關反向代理的設定，

```
server {
    listen         80 default_server;
    server_name    example.com www.example.com;
    root           /var/www/example.com;
    index          index.html;
    try_files $uri /index.html;
}

server {
    listen         80 default_server;
    server_name    example2.com www.example2.com;
    root           /var/www/example2.com;
    index          index.html;
    try_files $uri /index.html;
}

```

### 相關配置

主要的配置檔位置在 Linux 環境中會在 /etc/nginx 底下，除了 epoll 可以從機制上影響效能外，也可以從系統服務的性質從[核心的設定][3]以及相關設定優化效能。

- worker_processes 就是配合主機核心數，若是 4 核心就可以設置 `worker_processes 4`
- worker_rlimit_nofile 是可以開啟的檔案數量，像我以前公司伺服器就有需要提供圖磚的快取，設定大量的開啟檔案數量可以說是必須
- keepalive_timeout 等太久就切掉
- keepalive_requests 限制連線數
- 快取，在 Nginx 也有提供 proxy_cache_path 的快取設定
- Gzip 壓縮，這個在很多地方都可以啟用，不一定需要在 Nginx 開啟
- client_max_body_size 限制檔案上傳大小

如果有需要用到 websocket，設定會不太一樣，參考[官網文件][4]需要配置如下。

```
proxy_http_version 1.1;
proxy_set_header Upgrade $http_upgrade;
proxy_set_header Connection "Upgrade";
```

### Log

- access log 位置會在 /var/log/nginx/access.log，這邊推薦可以使用 [GoAccess][5] 將資訊圖像化，如果不想使用手工，可以搭配 Linux 的 [crontab 排程][6]進行自動更新。
- error log 位置會在 /var/log/nginx/error.log

### 反向代理

網域往往只能連到一台入口主機，但當我們後端有很多網站及服務分配到多台主機時，這時候就需要透過路徑上的代理來轉發還有配置附載平衡，Nginx 就提供了這樣的功能，當然 AWS 上也有提供相關服務。

另外附載平衡也很簡單，其中有幾種模式可以提供選擇，也可以進一步設定 health_check，讓 Nginx 去定時確認後端伺服器是否安好。

- least_conn 選擇最少連線數
- least_time 回應時間
- weight 倍數

```
upstream myapp1 {
    server srv1.example.com weight=3;
    server srv2.example.com;
    server srv3.example.com;
    least_conn;
}

server {
    listen 80;
      location / {
            proxy_pass http://myapp1;
      }
}
```

[1]: http://nginx.org/en/docs/events.html
[2]: https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-16-04
[3]: http://nginx.org/en/docs/ngx_core_module.html
[4]: https://www.nginx.com/blog/websocket-nginx
[5]: https://goaccess.io/
[6]: https://github.com/allinurl/goaccess/issues/703
