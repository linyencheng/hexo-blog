---
layout: post
title: "網站伺服器架設 (Ubuntu EC2)"
subtitle: "快速入門網站伺服器 (Ubuntu EC2) 的基礎設定"
date: 2018-10-04
author: "Lin Yen-Cheng"
catalog: true
tags:
  - Tools
---

### 網頁伺服器

前端來說，在這個大 SPA 時代，我們會需要放建置過的網頁檔到伺服器，讓伺服器能夠吐出我們的網頁到使用者端，所以需求就會是需要一個可以發佈檔案的位置，還有接收請求的相關程式以及域名、路由的規劃。

- https: 推薦使用 [certbot](https://certbot.eff.org/)，可以方便的使用免費的 https
- node.js: 推薦使用 nvm 來進行版本管理，升級就很簡單 `nvm install --lts` 然後 `nvm use --lts`
- pm2: 推薦使用 cluster mode 無痛升級效能，node.js 在 Linux 上運行的好物

### Linux 簡介

Linux 是一個檔案系統，所以檔案要有系統的被放置(可以這樣想像 xddd)

1. 常用的會有在 home 目錄底下的{使用者名稱}目錄，因為 linux 的權限規劃的比較嚴謹，每個檔案都有相關的存取權限，建議發佈位置放在這個目錄下。
2. etc (配置檔)
3. var(log)

### 主機設定

- 連接至主機，一般建議用 putty 免安裝版用 ssh 去連就可以了，如果虛擬機主人沒有動 port 預設都是可以連的，然後搭配不同家廠商的服務，大致都需要提供一些 key 或是配置檔。
- 升級系統相關套件與服務

```bash
sudo apt-get update
sudo apt-get upgrade
sudo apt-get dist-upgrade
```

- 清理安裝檔

```bash
sudo apt-get autoremove
sudo apt-get clean
sudo apt-get autoclean
```

### 程式與服務

當我們安裝了像是 Nginx、Jenkins、DB 後，會需要管理他們，在 Linux 中，我們一般會使用 `sudo systemctl` 加上底下幾種指令:

1. status (狀態)
2. stop (停止)
3. start (開始)
4. enable (開機啟用)
5. restart (重開)

底下的範例就是看 nginx 目前的狀態，接著列出有在開機預設開啟中的程式或服務，最後看目前有在執行的服務。

```bash
systemctl status nginx
systemctl list-unit-files | grep enabled
systemctl | grep running
```

這些服務的 log 一般都會存在 `/var/log` 資料夾中，如果我們想看 nginx 的 access log，就可以使用 `tail -f /var/log/nginx/access.log`，這裡有一點要注意的是如果沒有管理，log 可能會佔蠻多硬碟空間，這時候就可以使用 `sudo du -sh /var/log/` 來看到底用了多少，改善方法有以下兩種：

1. 從設定檔中進行設定`/etc/logrotate.d`
2. 執行指令進行清除 `sudo find /var/log/ -type f -regex '.*\.[0-9]+\.gz$' -delete`

### 排程

先寫好想要執行的 `test.sh` 檔，然後給權限 `chmod +x test.sh`，下面示範會產生資料夾:

```bash
#!/bin/bash
DIR=`date +%D`
DEST=~/test/$DIR
mkdir $DEST
```

自訂的排程時間則可以使用 [crontab][crontab]，使用 `crontab -e`進行編輯，透過分鐘、小時、日、月、星期幾來進行週期設定。

- 譬如每個星期二的 4:00am 就可以寫成 `0 4 * * 2 ~/test.sh`
- 也可以使用縮寫 `@daily ~/test.sh`

相關縮寫如下：

- @hourly `0 * * * *`
- @daily `0 0 * * *`
- @weekly `0 0 * * 0`
- @monthly `0 0 1 * *`
- @yearly `0 0 1 1 *`

防呆可以先用下面這個網站試寫:
https://crontab.guru/

### 程式發佈

- 放置檔案到伺服器
  1. FTP，架設的方式很多部落客都有教學
  2. jenkins，從版控抓 master branch 下來自動 build 及執行 shell 記得 `BUILD_ID=DONTKILLME`
- 接受請求的程式
  1. 靜態的檔案，這個部分推薦使用 nginx 就好了，方便又快速
  2. 如果是 node.js 的後端，需要使用 PM2 這類的工具進行管理

### 編輯器

`select-editor` 建議使用 nano，相對簡單，使用時就是使用指令加檔名 `nano test.conf`，若是要修改權限外的檔案則要 `sudo nano test.conf` ，常見的如下，vi 和 vim 有區分指令模式跟輸入模式，操作概念較為複雜:

- vi
- vim
- nano

### 問題排除

- 防火牆，會碰到這個一般是都弄好了，但怎麼都看不到網頁，若停用防火牆後以使用，代表[防火牆需要設定][ufw]。
  `sudo ufw disable` 停用
  `sudo ufw allow from 192.168.0.0/16` 允許通過
- 端口、埠 (Port) 的設定，可能程式沒寫好錯誤處理，又剛好選到的 port 跟其他服務一樣，也請看一下目前這個 port 是不是有被使用:
  `netstat -tulpn | grep LISTEN`
- 記憶體不足，如果記憶體不足當機的話，可以[幫 EC2 主機加上 swap][swap]，因為預設是沒有的。
  `top` 或 `htop` 看一下是不是一直在交換
  `watch -n 1 free -m`只看記憶體
- 檔案權限然後上傳有問題，會發現竟然寫不進去，全開的話就 chmod -R 777 ./\*，不需要執行的話就 755，執行 (+X)，這裡可以看到三個數字，就是搭配在 linux 上使用指令 ls -l 看到三組的 rwx，其中 r=4 (讀)，w=2 (寫)，x=1 (執行)

| User | Group | Other |
| ---- | ----- | ----- |
| 7    | 6     | 4     |
| rwx  | rw    | r     |
| 7    | 7     | 7     |
| rwx  | rwx   | rwx   |

[swap]: https://www.astralweb.com.tw/how-to-manually-add-swap-to-ubuntu/
[crontab]: https://www.digitalocean.com/community/tutorials/how-to-use-cron-to-automate-tasks-on-a-vps
[ufw]: https://www.peterdavehello.org/2016/01/ubuntu-based-gnulinux-firewall-ufw-essential-config/
