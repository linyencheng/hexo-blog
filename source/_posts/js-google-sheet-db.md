---
layout: post
title: "Google 試算表應用: 簡易資料庫"
subtitle: "利用 Google Visualization API, App script 將試算表變成簡易資料庫"
date: 2019-03-28
author: "Lin Yen-Cheng"
catalog: true
tags:
  - JavaScript
---

### Google 表單與試算表如何成為資料庫

之前在新創公司服務，由於老闆時程超級趕，高手前同事也就想出了這樣的辦法，很厲害的也把 CRUD 的函式都寫好，我接手的時候就只要叫用即可，過了兩年，發現在要和非資訊領域的人合作時，這也是個不錯的解決方案，Google 表單做到了可以防呆輸入，試算表則做到了同步功能，也可以動態額外的新增欄位，這個 Combo 技能如果在不要求效能，或是並沒有頻繁更新的情況下，覺得真的很方便，而且資料還可以同時很多人編輯。

### Visualization API

這篇文章有[簡易的介紹](https://www.wfublog.com/2016/11/google-4-sql.html)，主要就是透過更方便的 API 來操作 google 試算表，優點是方便好用，缺點就是別人知道了也可以任意改動我們的資料表了。

官方的參考文件:
https://developers.google.com/chart/interactive/docs/reference

### Google App Script

另外一個比較安全一點的方法就是，表單不開放，但透過 App script 去開出 API，缺點就是有些邏輯可能要稍微防呆一下，只用官方的 function 並無法完全符合需求且防掰。

網路上有很多比較複雜的範例，底下就實做了一個簡單方法，主要是去把表單裡的資料全部抓出來，並組成我們要的 JSON 檔。

假設試算表中第一列為資料欄位名稱，第二列才開始是資料，我們可以先跑迴圈把所有的欄位抓出來，再利用欄位的數量去把每一列的資料一一撈出並映射到物件中，最後把物件放進陣列就完成了。

官方的參考文件:
https://developers.google.com/apps-script/

```js
function getRows(sheet) {
  var jo = {};
  var dataArray = [];
  var result = {};
  var labels = [];
  var data = sheet.getDataRange().getValues();
  var rows = [];

  for (i in data) {
    if (!!data[0][i]) {
      labels.push(data[0][i]);
    }
  }

  // collecting data from 2nd Row , 1st column to last row and last column
  rows = sheet
    .getRange(2, 1, sheet.getLastRow() - 1, sheet.getLastColumn())
    .getValues();

  for (var i = 0, l = rows.length; i < l; i++) {
    var dataRow = rows[i];
    var record = {};

    for (j in labels) {
      record[labels[j].toString()] = dataRow[j];
    }

    if (record[labels[1]]) {
      dataArray.push(record);
    }
  }

  jo.results = dataArray;
  jo.status = 1;

  result = JSON.stringify(jo);

  return ContentService.createTextOutput(result).setMimeType(
    ContentService.MimeType.JSON
  );
}
```
