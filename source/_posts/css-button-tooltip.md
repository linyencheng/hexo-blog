---
layout: post
title: "幫按鈕加上提示(tooltips)"
subtitle: "用 CSS 幫按鈕加上 tooltip"
date: 2017-12-20
author: "Lin Yen-Cheng"
catalog: true
tags:
  - CSS
---

### 按鈕 X tooltip

在 [codepen][1] 上看到了一個關於 tooltip 簡單又聰明的做法，有用到偽元素跟中括號的選擇器，直接把原作者的原始碼放在底下，真的要我憑空寫還真的寫不出來，覺得好厲害 XD

比較特別的當然就是在按鈕的自定義了一個屬性`data-tooltip`，接著在樣式檔中透過一個中括號的選擇器來找到這個特殊屬性，取值的部分是透過 `attr(data-tooltip)` 來取~

偽元素則是 `:before` 及 `after` 透過在 element 的前後透過樣式檔插入元素 XD 其實只是一個是直接打這種透過樣式檔來插入~

[1]: https://codepen.io/cbracco/pen/qzukg

### Sample Code

```html
<button data-tooltip="內容">按鈕</button>
```

```css
/* Add this attribute to the element that needs a tooltip */

[data-tooltip] {
  position: relative;
  z-index: 2;
  cursor: pointer;
}

/* Hide the tooltip content by default */

[data-tooltip]:before,
[data-tooltip]:after {
  visibility: hidden;
  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
  filter: progid: DXImageTransform.Microsoft.Alpha(Opacity=0);
  opacity: 0;
  pointer-events: none;
}

/* Position tooltip above the element */

[data-tooltip]:before {
  position: absolute;
  top: 0px;
  left: 160px;
  margin-bottom: 5px;
  margin-left: -80px;
  padding: 7px;
  width: 180px;
  border-radius: 3px;
  background-color: #000;
  background-color: hsla(0, 0%, 20%, 0.9);
  color: #fff;
  content: attr(data-tooltip);
  text-align: center;
  font-size: 14px;
  line-height: 1.2;
}

/* Triangle hack to make tooltip look like a speech bubble */

[data-tooltip]:after {
  position: absolute;
  top: 5px;
  left: 75px;
  margin-left: -5px;
  width: 0;
  border-right: 10px solid #000;
  border-right: 10px solid hsla(0, 0%, 20%, 0.9);
  border-top: 8px solid transparent;
  border-bottom: 8px solid transparent;
  content: " ";
  font-size: 0;
  line-height: 0;
}

/* Show tooltip content on hover */

[data-tooltip]:hover:before,
[data-tooltip]:hover:after {
  visibility: visible;
  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
  filter: progid: DXImageTransform.Microsoft.Alpha(Opacity=100);
  opacity: 1;
}
```

如果是 React 的開發著的話，也推薦下面這個函式庫，可以減少我們很多的開發和維護。

https://github.com/wwayne/react-tooltip
