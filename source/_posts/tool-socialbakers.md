---
layout: post
title: "小編救星: 粉專按讚分析與比較"
subtitle: "運用 SocialBake 上提供的資料，以下方法僅供個人研究與觀察，非商業用途使用"
date: 2019-08-15
author: "Lin Yen-Cheng"
catalog: true
tags:
  - Thinking
---

### SocialBakers 資料

首先推薦 SocialBakers，繼上次寫了[社群觀察][social-media]後，這次來看如何抓取資料來做後續觀察和應用。
找到我們想要觀察的對象後，接著就是發揮宅宅之力，這次找了下面兩個連結:

1. [連結一][thai]
2. [連結二][ko]

![hicharts-data](hicharts-data.png)

由於宅宅之力發揮的關係，我們可以輕鬆看出這裡使用的函式庫，所以我們也就有機會拿到資料了，就不用一筆一筆手動輸入 (感人)。

### 資料抓取與匯入

所以我們嘗試使用下面的程式碼來抓出資料。

```js
var yValues = chart.series[0].data.map(data => [data.y]);
JSON.stringify(yValues);
```

抓到之後，我們可以透過 google 開放的 App Script，把剛剛得到的資料貼進來執行，就能夠匯入成功。

![google-app-script](google-app-script.png)
![sheet-analysis](sheet-analysis.png)

程式碼如下，第一欄為日期，所以我們從第二欄開始匯入。

```js
function myFunction() {
  var arrayKo = [
    [2126900],
    [2127537],
    [2127869],
    [2127856],
    [2127684],
    [2127530],
    [2127575],
    [2127604],
    [2127494],
    [2127392],
    [2127441],
    [2127469],
    [2127468],
    [2127453],
    [2127504],
    [2127529],
    [2127784],
    [2127701],
    [2127609],
    [2126818],
    [2126062],
    [2124417],
    [2120241],
    [2118302],
    [2117611],
    [2109668],
    [2099853],
    [2092609],
    [2088067],
    [2071181],
    [2050875]
  ];
  var arrayThai = [
    [2437107],
    [2437495],
    [2437827],
    [2438062],
    [2438283],
    [2438526],
    [2438877],
    [2439208],
    [2439553],
    [2439795],
    [2439998],
    [2440106],
    [2440274],
    [2440465],
    [2440566],
    [2440688],
    [2440992],
    [2441390],
    [2441917],
    [2442219],
    [2442444],
    [2442929],
    [2444325],
    [2444827],
    [2445387],
    [2445720],
    [2446088],
    [2446396],
    [2447244],
    [2448251],
    [2448905]
  ];
  var sheet = SpreadsheetApp.getActiveSpreadsheet().getActiveSheet();
  //  getRange(row, column, numRows, numColumns)
  sheet.getRange(2, 2, 31, 1).setValues(arrayKo);
  sheet.getRange(2, 3, 31, 1).setValues(arrayThai);
}
```

### 提醒

這個部分還是建議使用在臨時想要個人觀察或做報告上，不要再進一步拿去做商業使用，如果有長期需求還是建議使用付費版本，支持一下好的平台。

[social-media]: https://linyencheng.github.io/2019/08/13/thinking-social-media-inside/
[thai]: https://api-sbks.socialbakers.com/charts/fb-pages/fans.html?ids=46251501064&interval=last_month
[ko]: https://api-sbks.socialbakers.com/charts/fb-pages/fans.html?ids=136845026417486&interval=last_month
