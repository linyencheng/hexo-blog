---
layout: post
title: "Javascript 樣板簡介"
subtitle: "Javascript template, EJS"
date: 2017-11-06
author: "Lin Yen-Cheng"
catalog: false
tags:
  - JavaScript
---

### 樣板引擎

當網頁使用 SPA 的架構，又需要在社群工具分享時顯示個別的內容時，有個比較直觀的方法是可以在 Server render 時產生的 html string 使用 node 內建的 [util.format()][1] 去取代相關變數，但當變數一多的時候:

```js
util.format(final, x1, x2, x3, x4, x5);
```

然後就想說有沒有個樣板引擎，由於後端目前是使用 express，所以當然先從 [express 文件][2]開始研究，其中官方建議的是 jade，但後來發現需要花點時間學習特殊語法 Orz 所以就查查其他解決方案，有看到另外一個也蠻多人用的就是 EJS，看起來跟 JS 差不多，好上手，最後就決定看這套怎麼用啦，使用時必須設定一下:

```js
// 設定相關位置
app.set("views", path.join(__dirname, "views"));
// res.render 時使用解析引擎
app.set("view engine", "ejs");
// 使用物件傳遞參數
res.render("index", { x1, x2, x3, x4, x5 });
```

然後就可以直接在裡面 `<%-` 取 unescaped value，[文件中有其他的詳細介紹][5]

```html
<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#">
  <head></head>
  <body>
    <script>
      childValue = "<%- x1.childValue %>";
    </script>
  </body>
</html>
```

最後如果要在使用社群工具分享個別頁面時顯示個別的內容，就需要依照[Open Graph protocol][3]的規則，搭配上面說的樣板引擎，這樣就可以在最後生成網頁時，塞進需要的資料，達到產生動態資訊的效果，最後的結果可以透過 [facebook 提供的偵錯工具][4]來看~

[1]: https://millermedeiros.github.io/mdoc/examples/node_api/doc/util.html
[2]: http://expressjs.com/zh-tw/guide/using-template-engines.html
[3]: http://ogp.me/
[4]: https://developers.facebook.com/tools/debug/sharing/
[5]: http://ejs.co/#docs
