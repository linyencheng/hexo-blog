---
layout: keynote
title: "作品集"
subtitle: "簡單介紹之前做過的東西"
iframe: "https://linyencheng.github.io/portfolio/"
date: 2018-01-24 13:00:00
author: "Lin Yen-Cheng"
tags:
  - Thinking
---

### [Watch Slides →](https://linyencheng.github.io/portfolio/)

<img src="https://i.imgur.com/5q3kLeS.png" width="350" height="350"/>

<small class="img-hint">可用手機掃描觀看</small>

### 相關實作

> 1. [咖啡地圖][coffee map]：串接[開源資料][cafenomad] (React)
> 2. [高鐵班次][thsr]：串接[公開資料](https://ptx.transportdata.tw/MOTC/Swagger/#/THSRApi) (React)
> 3. [對戰紀錄表][pokemon competition]：串接 Google Sheet (React)
> 4. [單頁筆記][vue js note]：Vue.js SPA (Vue)

#### 良師塾事業有限公司, 前端工程師, 2018/1 ~ 至今

> 1. 負責整個[線上教室][online classroom]、[智慧題庫][wisdom bank]前台 (React)
> 2. 開發線上教室討論區後端、即時答題系統 (Socket.io)
> 3. [良師塾人文食飲][literati cafe]站台優化 (WordPress)
> 4. 良師塾人文食飲[人力招募][hiring]、[排班系統][cafe shift]的功能開發及維護 (React)
> 5. [良師塾抽兌獎系統][lucky draw] (React)
> 6. 導入 Trello，建立 CI/CD 環境與測試 (Jenkins)

#### 众社企股份有限公司, 前端工程師, 2017/4 ~ 2018/1

> 1. 開發[友善旅館登錄系統][friendly hotel registration system]，方便非資訊人員進行相關資料維護 (React)
> 2. 實作[友善旅館][friendly hotel]伺服器渲染 SSR、多國語系，改善效能及 SEO (React)
> 3. 開發[新北醫藥通-健康地理資訊查詢系統][medicare] (GIS, Vue)

#### 北宸科技股份有限公司, 軟體工程師, 2015/9 ~ 2017/10

> 1. 開發圖資更新網站：包含會員、購買、下載、多國語系 (Vue)
>    [Saipa Website][saipa website]、[Kia Website][kia website]、[Hyundai Website][hyundai website]
> 2. 開發 [Web GIS 前端圖台 SDK][web gis sdk] (Openlayers)
> 3. 建構 [Web GIS][web gis] 平台，配置系統架構(cache)、部署圖資、API 設計 (Java)
> 4. 開發 Token-Based 會員系統 (Java)
> 5. 4 月中~10 月中為 part-time 協助專案維護及相關諮詢

#### 碩士論文

> 群眾外包應用於藥物服用知識系統之研究: 透過群眾外包的概念，設計出便於使用者分享用藥經驗及了解相關知識的平台 Medication Knowledge System (jQuery Mobile, ASP.net MVC)，並透過 UTAUT 模型來驗證。

<div style="text-align: center;">
<iframe src="//www.slideshare.net/slideshow/embed_code/key/b78ZWnjOuIN5cB" width="425" height="355" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/ssuser2bece7/ss-154311857" title="群眾外包應用於藥物服用知識系統之研究" target="_blank">群眾外包應用於藥物服用知識系統之研究</a></div>
</div>

[google marketing]: https://goo.gl/wfyrkV
[online classroom]: https://onlineclassroomdaily.liangshishu.com
[lucky draw]: https://luckydraw.liangshishu.com/
[friendly hotel]: https://ourhotel.azurewebsites.net/
[friendly hotel registration system]: https://ourcitylovewebapps.azurewebsites.net/hotelapp/
[web gis sdk]: https://map.polstargps.com/polnavMapAPI/
[web gis]: https://map.polstargps.com
[saipa website]: http://saipa.polstargps.com/
[kia website]: http://kia.polstargps.com/
[hyundai website]: http://hyundai.polstargps.com
[medicare]: https://www.health.ntpc.gov.tw/medi
[wisdom bank]: https://wisdombank.liangshishu.com/
[literati cafe]: https://literaticafe.liangshishu.com/
[hiring]: https://hiring.liangshishu.com/
[cafe shift]: https://cafeshiftarrangement.liangshishu.com
[thsr]: https://linyencheng.github.io/thsr-app
[vue js note]: https://linyencheng.github.io/vue-note
[coffee map]: https://linyencheng.github.io/coffee-map
[pokemon competition]: https://linyencheng.github.io/pokemon-competition
[cafenomad]: https://cafenomad.tw/developers/docs/v1.2
