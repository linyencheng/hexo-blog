---
layout: post
title: "解密網路行銷工具"
subtitle: "淺談 facebook pixel 與 google tag manager 的結合與應用"
date: 2018-10-14
author: "Lin Yen-Cheng"
catalog: true
tags:
  - Marketing
  - Tools
---

### 關於再行銷

天啊，我被綁架了嗎? (最近越來越有種在網路上很裸露的感覺。

事情是這樣的，主要發生了兩件事:

- 最近點了一下藍鯨的機票，結果臉書上就不小心看到了藍鯨機票的特價訊息 (來自 skyscanner(幹連地點都這麼準(靠腰也太裸露了吧。
- 過了一陣子，因為最近發現溝通成本過高，時間限制下不得已選擇放棄隊友，開始慢慢自己 carry 後端，於是多讀了一點 node.js 的東西，然後就在 Youtube 上看到了 The node js master 的課程推薦(幹連語言都這麼精準。
- 其他大概就是點過購物網站後，hotmail 的側欄會出現上次觀看的類別(像是最近剛逛了 ssd 所以就被推 3c，或是臉書裡面會看到 ssd 的特價訊息

> 才慢慢驚覺，天啊，原來這就是廣告，我們就這樣被貼標籤了。

### facebook pixel 與 google tag manager

那究竟是在什麼環節上被偷偷地貼標籤，答案就是當我們瀏覽網頁並進行動作的時候。在好奇心驅使下，剛剛我也產生了一個 [pixel][1]，並且在 [google tag manager][2] 中的觸發條件設定了所有網頁瀏覽，然後神奇的就可以在 Facebook 的監控頁上看到了。也就是說我們設計了一個情境，並把情境跟 Facebook 結合，這樣在觸發的時候，Facebook 就可以得到相關的訊息，進而進行後續的廣告活動，也就是大家常聽到的 Campaign 。

> 沒錯，出賣我們的其實是我們自己的動作，還有我們常被推薦的那個網頁。

### 核心觀念

再來每個 Campaign 都會依照需要來決定這次的 Objective:

- Branding 也許需要的只是流量，下的條件可能相對簡單，像是 25~30 歲位於台南，計費也許是用 CPC (Cost per click) 來計費
- Retargeting 已經付了比較貴的廣告費，也針對特定的目標對象進行再行銷，也就是打了一次，再打一次 xddd 精準的通常都會期待 TA 能有進一步的發展，比較可能就是用 CPA (Cost per action) 來計費
  - Behavioral targeting 比較進階一點透過使用者過去瀏覽網路的行為及路徑，找出潛在消費需求 (像是中槍的機票推薦，進一步也可以把廣告素材綁到[廣告識別碼上面][3]
- 當然 APP 的話還有 CPI (Cost per install)

後來好奇點了一下廣告上面的小按鈕，才發現竟然不是 google 而是 criteo，目前 EC (電子消費) 的廣告投放幾乎都是他們的天下。

那怎麼將訊息打到目標對象，首先還是需要定義目標對象，然後研究他們

- 何時上網?
- 喜歡什麼？
- 什麼人事物對目標對象有影響力？
- 使用什麼社群媒體？

上面這些才是核心，然後在不同的媒介上投放的訊息就會有所不同，最重要的是統一風格的重要，統一可以表現出一個形象，只要形象打到了，就會知道是你。像是品牌標誌的使用方式，字形或色彩，比較容易忽略的是語調，這樣才不會跳痛或是覺得是不是小編請假了，在很多社群媒體上，我們下意識都會先看到圖才看到帳號，所以讓別人記得你的形象，這才是品牌。

### 要怎麼避免裸露

使用無痕，這樣我們的 [Fingerprint (註)][4] 會不一樣，比較變態的[殭屍 cookie][5] 好像不常見所以就忽略了? 另外也可以選擇使用上古神器，也就是地表最強 Chrome 的安裝器 IE xddd 聽說因為真的太爛了導致很多功能都不支援也就不會有後續問題了 xddd

註: Fingerprinting is a technique, outlined in the research by Electronic Frontier Foundation, of anonymously identifying a web browser with accuracy of up to 94%.

[1]: https://www.facebook.com/business/help/952192354843755
[2]: https://tagmanager.google.com
[3]: https://support.google.com/authorizedbuyers/answer/3221407?hl=zh-Hant
[4]: https://github.com/Valve/fingerprintjs
[5]: https://en.wikipedia.org/wiki/Zombie_cookie
