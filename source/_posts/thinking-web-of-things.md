---
layout: post
title: "Web of Things"
subtitle: "前端 X IoT = WoT?"
date: 2018-05-07
author: "Lin Yen-Cheng"
catalog: true
tags:
  - Thinking
---

### 波的小常識

故事剛開始就從射頻開始談起吧? 簡單來看在各種物聯網的情境下，最容易出現的干擾大致可能有：

- 遮蔽太厚
- 裝置電量有限
- 2.4G 干擾
- 大電流干擾

關於遮蔽的問題，可以回到波的特性來看，

- 高頻: 可以短時間內傳輸較多的訊號
- 低頻: 傳輸速率較慢但有比較高的穿透率

雖然可以透過調整波形震幅或形狀來達到同個頻率傳遞更多訊號，但同樣條件底下頻率高的傳遞訊號的速度較快，若是需要傳遞同樣的距離則需要更多更大的能量，下圖為本次 LPWAN 主題相關的資訊：

http://iotbusinessnews.com/WordPress/wp-content/uploads/LPWAN-technologies-chart-range-vs-datarate.jpg

### 電波端協定

["智慧電表和家庭閘道器之間的這段通訊與資料傳輸路徑，就是所謂的 B Route，目前傾向於採用具有低功率、抗干擾、長距離傳輸等特性的 Sub-GHz 低功率廣域網路技術(Low Power Wide-Area Network, LPWAN)，例如 LoRa、SigFox、NB-IoT 及 Wi-SUN(Wireless Smart Utility Network)等。 "][summary]

| 遠傳        | 中華電 | 台灣大       | 亞太電信 | 台灣之星 |
| ----------- | ------ | ------------ | -------- | -------- |
| NB-IoT,LoRa | NB-IoT | NB-IoT,LoRa? | LoRa     | NB-IoT   |

[lora][lora] 看各家目前技術選型感覺應該是一個備胎的概念 xddd lora 是 long range 主打長距離， 5km up 的樣子?

[sigfox][sigfox] 用宅配來打比方的話相當於機車宅配便利袋的概念，當送的貨物(資料)較少時就不需要開貨車來送，耗的能量極少當然傳輸資料也很少(1w 5km)，目前中興保全、Obike 好像已經開始使用了，據說有[040 開頭的電話][sigfox]就是了，算是第一個台灣取得通訊協定執照的？很積極的搶地盤，但這種後發搞不好才有後發優勢，想想當年偉哉 Wi-MAX...

Wi-SUN (Wireless Smart Utility Network)，目前日本人用在智慧電表的，920MHz 的低頻穿透能力佳傳輸距離可以到達 1km，日本人讚讚讚? 也是 next drive 採用的技術~

### 傳輸端協定

接著就是比較上層的軟體實作，考量到裝置特性:

- 每次傳輸量為較小封包(溫度濕度用電量等等)
- 裝置數量巨大
- 快速
- 省電

目前有幾家領頭羊都覺得自己好棒棒？

- Weave (google)
- AMQP (M\$)
- MQTT (amazon 三星)
- HomKit (蘋果)

### Web of Things

為什麼除了 IoT 以外又多了一個名詞? 先從 IoT 開始看起 IoT 是 Internet of Things 的縮寫，但是當 Things 都連上網之後呢？我們又要怎麼跟 Things 做溝通?

可能的問題：

- 裝置的數量會大量上升
- 裝置的電量有限
- 裝置可能被放置在訊號不佳的位置
- 裝置成本會被限制

[Web of Things][webofthing] 在 Mozilla 的相關文件介紹中定義了可讀性較高的 json 格式，一個 geojson 的概念，未來也許會叫 wotjson ? 也定義了 REST 的 API，底層是透過 WebSocket 來保持住連線~

目前大多數智慧家庭都是需要一個控制中心，然後透過 App 跟控制中心連結，接著才間接控制裝置，next drive 這家優秀的公司推出了一個 [cubej][cubej]，是一個 HEMS Gateway，除了透過 App 也可以透過 Line 來直接的操作，在 App 過多的這個時代，覺得真的是一個高手級的想法，如果想取得裝置的影片更可以直接傳到 Line 裡，並無改變消費者的習慣，簡直偉大設計 RRR~

智慧音箱，可能是另外一個劃時代的發明了，自從 siri 跟 ok google 出現後，AI 的應用開始越來越多，智慧音箱在對面的發展簡直海放呆丸不知道幾年? AI 有個小關鍵就是需要訓練資料，對岸的資料是幾個億當單位的 Orz 而且是中文！智慧音箱或許也是一個好的 Gateway 的具現化方案，那前端之於物聯網能扮演什麼樣的腳色，也許是搭配熟悉的 REST 來實作一個控制介面，也能將資訊圖像化，或著是透過聊天機器人的限制範圍問答來達到控制裝置? 不過不管未來怎麼樣，呆丸大概都是落後的吧 QQ

[webofthing]: https://mozilla-iot.github.io/wot/
[cubej]: https://www.nextdrive.io/en/product/Cube-J
[summary]: http://www.2cm.com.tw/coverstory_content.asp?sn=1802120004
[lora]: http://3smarket-info.blogspot.tw/2017/04/lorawan-lora.html
[sigfox]: https://www.bnext.com.tw/article/46514/sigfox-launch-its-iot-service-in-taiwan
[nbiot]: https://chinese.engadget.com/2017/11/27/fareastone-nb-iot-tw/
