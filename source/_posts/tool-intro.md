---
layout: post
title: "網站導覽設計 (使用 Intro.js)"
subtitle: "使用 intro.js 輕鬆幫網站 UI 加上導覽"
date: 2018-10-01
author: "Lin Yen-Cheng"
catalog: true
tags:
  - Tools
  - JavaScript
---

### Intro.js

網路上發現的好工具，當介面做得很爛?(誤)，需要導覽的時候，除了圖示說明或拍影片以外，就是使用類似這樣的引導工具了，能夠透過按照順序的彈跳說明來引導使用者，以下是官方的 Demo 連結。

[https://introjs.com/example/hello-world/index.html](https://introjs.com/example/hello-world/index.html)

### 使用方法

1. 安裝最快的方法就是 cdn
2. 在需要說明的檔案中加入適當的 attribute，像是 data-step 或 data-intro
3. 有很多選項可以透過[配置參數設定](https://introjs.com/docs/intro/options/)

```js
introJs()
  .setOption("nextLabel", ">>")
  .setOption("prevLabel", "<<")
  .setOption("skipLabel", "我要跳過!")
  .setOption("doneLabel", "完成 >.^")
  .start()
  .onexit(callback);
```

### 小結

要先做完所有的設定才能跑 start() ，請記得，順序很重要，使用上完全無痛，只要啟動說明前記得先判斷 introJs 是否存在，這樣在 cdn 失敗或是想要拔掉的時候比較不會出現錯誤。
