---
layout: post
title: "開發者工具網頁除錯教學 (Debug)"
subtitle: "淺談使用網頁開發者工具在 HTML/CSS/Javascript/API 除錯的方法"
date: 2019-06-28
author: "Lin Yen-Cheng"
catalog: true
tags:
  - Thinking
  - HTML
  - JavaScript
  - CSS
---

### 清除網頁快取

了解情境並重現錯誤是第一步，再來當發布到測試環境時，因為網站會在很多地方有快取機制，所以建議底下幾種方法刷新頁面:

1. 用 `Ctrl+F5` 進行強制更新。
2. 按 f12 開啟開發者工具之後，對瀏覽器上的重新整理按右鍵，開啟選項後選最下面 empty cache and hard reload。
3. 按 f12 開啟開發者工具之後，去 Application 中清除。

### HTML 除錯 (Debug)

開發靜態網頁時，可能會遇到下面的問題：

1. 存完檔後就需要重新整理頁面來看結果。
2. 資源檔路徑的設定可能在 local 和 server 上有差異。

解決方案就是在開發時：

1. 使用 [livereload][1] 及他的 [chrome 外掛][2]。
2. 使用 Chrome 提供的 [Web Server][3]。

### CSS 除錯 (Debug)

在樣式檔的開發上：

1. 按 F12 開啟開發者工具之後，打開之後 Elements 這個 tab 就可以讓我們看到所有針對這個樣式的設定，包含 Box modal 的示意圖，在用戶端就可以透過直接修改來看更動效果。
2. 按 F12 開啟開發者工具之後，使用模擬器模擬各種裝置大小。
3. 按 F12 開啟開發者工具之後，手機開啟開發者模式，使用 Chrome 開發者工具的 remote devices，就可以使用手機的 chrome 去 Debug。
4. 防呆上，也建議使用[編輯器的相關外掛][4]像是 Sass Lint，可以幫我們避開不少問題。

### Javascript 除錯 (Debug)

在開發 Javascript 的時候，由於是直譯的程式語言，所以打了什麼就會立即執行，這樣的特性使我們需注意盡量不要使用或產生 Global 變數，避免覆蓋的問題。若我們是有使用到 webpack，在除錯的時候就可以善用 `process.env.NODE_ENV === 'development'` ，透過這樣的判斷，可以在開發的狀態時，預先加入特殊條件及預設值，可以減少許多人工重複的部分。另外透過在 Debug 的時候善用 `console` 留下紀錄並搭配[`debugger;` 指令][6] 來執行瀏覽器上可用的除錯功能（一般為中斷點）。

1. console.log() 記錄
2. console.error() 顯示錯誤紅色
3. console.info() 顯示資訊
4. console.warn() 顯示警告黃色
5. console.clear() 清除 console
6. console.time() + console.timeEnd() 計算範圍內的物件耗費間
7. console.table() 適用陣列的內容

想要在 production 的時候自動隱藏 `console.log` 內容的話，建議可以先封裝，然後針對環境去做進一步的控制。另外不想一直重新整理的話，hot reload 就很重要：

1. 瀏覽器端：目前只要是使用主流的框架或是函式庫都會幫我們配置好
2. 伺服器端：需要使用 `nodemon` 這樣的工具，並在執行的時候告訴工具監看檔案的變化 `nodemon --watch ./src/* server.js`

編輯器也建議使用[編輯器的相關外掛][4]，特別是 ESLint (eslint-plugin-react 及 eslint-plugin-react-hooks)，React 開發者，可以使用官方文件中 error boundaries 的概念，一來不會讓整個網頁掛掉，二來可以在發生錯誤時，將相關資訊送回後端記錄，另外在 production 環境因為通常會進行[程式碼的壓縮及醜化][7]，所以若是需要在 Production 環境中偵錯的話，要記得一併[輸出 .map 檔][8]。

### API 除錯 (Debug)

前端在發[非同步請求][9]時通常會使用到 Promise，在實作時記得加上 catch，否則少數狀況會 `Uncaught (in promise)`。那在實作上 Postman 是個方便的好工具，可以先測試 API 是否正常，真的遇到問題時，如果是 node.js 撰寫的後端，在瀏覽器中輸入 `about://inspect`，我們就可以使用瀏覽器來看伺服器的 log，介面會比較友善一些。再來就是資料的確認，這時候推薦可以使用線上的 JSON Formatter and Validator。網頁部分會需要開發者工具中的 Network，我們就可以看到伺服器回覆的狀態碼以及 Response，然後針對相關狀態及回應進行處理，底下是相關代碼的簡易分類：

1. 200 成功
2. 4XX 用戶端的問題
3. 5XX 伺服器端的問題

[1]: https://www.npmjs.com/package/livereload
[2]: https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei?hl=zh-TW
[3]: https://chrome.google.com/webstore/detail/web-server-for-chrome/ofhbbkphhbklhfoeikjpcbhemlocgigb
[4]: https://linyencheng.github.io/2019/06/25/tool-vscode-extensions/
[5]: https://reactjs.org/docs/error-boundaries.html
[6]: https://developer.mozilla.org/zh-TW/docs/Web/JavaScript/Reference/Statements/debugger
[7]: https://developer.mozilla.org/zh-TW/docs/Tools/Debugger/How_to/Pretty-print_a_minified_file
[8]: https://linyencheng.github.io/2019/02/27/react-create-script-2/
[9]: https://linyencheng.github.io/2017/04/20/js-http-client/
