---
layout: post
title: "全文檢索 (Solr, Lunr)"
subtitle: "全文檢索相關名詞及概念理解(Solr, Lunr)"
date: 2018-04-06
author: "Lin Yen-Cheng"
catalog: true
tags:
  - Tools
  - Back-End
  - JavaScript
---

### 觀念解析

認真一點舉例好了 Orz 以前高中讀的單字書，印象中只有分級沒有經過排序，但可能有單字間的相關性？是為了方便記憶及背誦？不確定，但這不是重點，重點是就算給你單字書去考指考，也可能會因為查答案查太久而時間不夠？

字典就比較不一樣，字典會有個按照發音或是筆劃所建立目錄或索引，而透過排序過的索引，能夠進一步增加我們查閱的速度，延伸這樣概念，也就出現了[全文檢索引擎][1]這樣的產物了，全文檢索的核心概念就是索引，索引把散亂的東西結構化並排序，就是[全文檢索引擎][1]在做的事了...

生活上來說就像平常我的衣服是亂糟糟整坨放床上的 XD 但透過這種概念，我會把它分成短袖長袖短褲長褲... 這樣每次要跑步找短褲會比較快，直接到短褲區找就好了，當然破萬筆的索引沒那麼簡單，主要是透過分詞，然後進行反向索引來加速搜尋的過程。

### Solr

最後重點，推薦!!! Solr 好用 XD 實作上也相當的簡單? 關鍵就是要[餵原始資料][2]然後告訴 Solr 要將哪些欄位[建立索引][3]，未來就可以直接透過索引來找資料了，厲害的 Solr 也有提供[空間資料的檢索][4]，透過定義經緯度 ( lat, lng )，就可以簡單的做出搜尋我附近的美食這種功能惹，簡單吧 XDDD

### Lunr

如果需要在前端使用全文檢索，Lunr 提供了一個輕量化的函式庫，當然分詞的部分是以英文為主，網路上就有人提供了一個中文的分詞，開源的時代真的是很需要大家互相協助，希望有一天自己也可以。

https://github.com/Wiredcraft/lunr-chinese

而我也在自己實作的[咖啡地圖中](https://github.com/LinYenCheng/coffee-map)採用了這個檢索，原因我並沒有實作後端，後端是串接開源的資料，每次縣市回來就是 500 筆以上，於是才想說可以使用看看這個函式庫。

開源的資料：
https://cafenomad.tw/developers/docs/v1.2

[1]: https://www.ithome.com.tw/voice/90361
[2]: https://lucene.apache.org/solr/guide/6_6/using-solrj.html
[3]: https://examples.javacodegeeks.com/enterprise-java/apache-solr/solr-schema-xml-example/
[4]: https://lucene.apache.org/solr/guide/6_6/spatial-search.html
