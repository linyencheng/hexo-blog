---
layout: post
title: "伺服器效能優化"
subtitle: "使用 nginx 以及 pm2 cluster"
date: 2018-08-20
author: "Lin Yen-Cheng"
catalog: true
tags:
  - Back-End
  - JavaScript
  - Tools
---

### 系統設定

依照後端的用途，預設的 tcp timeout 時間可能不一定適合，為了效能可以進一步設定:
`sudo sysctl -w net.ipv4.tcp_keepalive_time=110`
`sysctl -p`
``

### 伺服器端

IO 上 [Linux 有個 File Descriptor][file descriptor]，一個 process 開啟一個檔案時，會建立一個參照去指向檔案，演算法則是針對這個 FD 進行處理。像是 select：
`int select(int n, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, struct timeval *timeout);`

- Apache 的 select (blocking)：監考老師問學生，隨堂考試寫完沒，收集足夠後送批改。同時太多學生時，會需要等學生回覆，上限預設 1024。
- Nginx 的 epoll (non-blocking)：老師不再問學生，學生完成後放講桌，收集足夠後送批改。所以當我們在 non-blocking 的情境底下，伺服器端一定會有回覆，但不一定會有資料 (待批改清單)，像是 Nginx 就可能因為本身設定的 timeout 就回覆說，您抽的號碼已經超過了，請重新領取號碼牌，是解決 C10K 的一個解決。

### 使用 Nginx

啟用 epoll，可以解決 select 機制下 server c10k 的問題，除此之外還可以做負載平衡，反向代理該設的參數要設對，[proxy_http_version 1.1 請注意][1]，有人有踩過雷了 xddd

Node.js 在建置的時候，要記得把 NODE_ENV 設定為 production，網路上的大大[測試][2]好像有差，再來是無痛解問題的 nginx，也是可以無痛增加效能的，像是如果配合 cpu 的核心數設定完全 Combo 技概念，[大幅增加效能][3]， linux 的虛擬機請下 lscpu 看幾核心？再來是系統比較底層的參數，像是下面的指令:
`sysctl -w net.core.somaxconn=512`
`sysctl -p`

再來就是壓縮和快取，壓縮可以做在 nginx gzip or node.js compression 這個 middleware 也可以，不過建議是 [nginx 設一設就好了][4]，快取，nginx 快取就是設定快取存放[位置跟時間][5]，由於是代理的關係，抓回來的東西如果很常被拿，就不需要再去要一次，會快很多。

### Node.js 應用程式管理工具

如果是 node.js 的後端，Linux 的話會需要註冊成系統服務或是使用 FOREVER, PM2 這類的工具，我常用的是 pm2 ，常用的指令可能像下面
`pm2 monit`
`pm2 serve`
pm2 的 [cluster mode (類似 nginx 配合多核心的設定)(無痛升級效能)][6]

測試的話可以用 jmeter 去打。

### API 快取

使用 [express-redis-cache][8]，如果是較為靜態的資料，就可以透過將資源短暫快取在記憶體中，可以減少對資料庫的負擔，也可以加快響應的速度。

### 資源檔

減少資源檔直接向伺服器取用，部分主題提供將腳本或是樣式檔編譯的功能，圖片相關資源檔則可以盡量使用 CDN 來提供，

[1]: https://www.facebook.com/pcman.im/posts/1835852189787783?__xts__[0]=68.ARCoZOGddo0-HZVCzZuTqrL6Jrh7KBC6JY0TuSerbNQwyuAsqZ6WDWHLBcYZHD1Mb6FqiGFk0g6wk9UQGdOjU6nAkrIuq1aqB2d_oXL-2SAYCCkVYiHlVd5cWv3Oi7nYTfDXkOus73vs&__tn__=-R
[2]: https://www.dynatrace.com/news/blog/the-drastic-effects-of-omitting-node_env-in-your-express-js-applications/
[3]: https://www.nginx.com/blog/tuning-nginx/
[4]: http://nginx.org/en/docs/http/ngx_http_gzip_module.html
[5]: https://serversforhackers.com/c/nginx-caching
[6]: http://pm2.keymetrics.io/docs/usage/cluster-mode/
[7]: http://expressjs.com/zh-tw/advanced/best-practice-performance.html
[8]: https://github.com/rv-kip/express-redis-cache
[File Descriptor]: https://zh.wikipedia.org/wiki/%E6%96%87%E4%BB%B6%E6%8F%8F%E8%BF%B0%E7%AC%A6
