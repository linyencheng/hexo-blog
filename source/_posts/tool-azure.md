---
layout: post
title: "Azure 網站管理"
subtitle: "Azure, Kudu, Bitbucket"
date: 2017-10-12
author: "Lin Yen-Cheng"
catalog: true
tags:
  - Tools
---

### Web App

如果利用 Azure 來當作網站服務的平台，平常開發時只會用到 Slot 中的 [Web App][1]，簡單的設定就可以跟常用的版控工具做持續發布，可以跟版控像是 bitbucket 連接，連接的流程就是透過 Azure 介面登入 bitbucket 然後選擇專案和需要同步的 branch 就可以了，由於是使用 Node 當 server 目前看起來是設定 web 的 config 檔就可以跑起來，如果測試到一半發現有些單機沒測出來的問題，沒關係也可以重新 Deploy 之前的版本~

### App Service

上線的服務則會使用 [App Service][2]，App Service 的部分則由剛剛的 Web App 交換過來，這是什麼意思呢？就是說當測試網站測試 OK 後就直接原封不動將專案置換成 App Service，有問題也可以隨時再換回來，若需要 CORS 也可以很方便的設定~

### 網站管理

那為什麼會突然提到網站管理呢？這次會用到的原因是因為跳警告說我們用的方案記憶體用量即將爆炸，但又從來沒有用過雲端的，也不知道從何看作業系統或是容器狀態，所以這次想找出時間點或是如何監看 Server 端狀態的工具，如果只需要看七天內的系統狀態，可以直接點儀表板進入 Metrics，可以看到 CPU 資料流量記憶體用量請求次數統計等等，那如果需要看一些 log 那就需要進階一點的服務：[Kudu service][3]，由於這篇介紹很詳細了，所以以下略過？(誤) 其實只要在 "專案名稱.azurewebsites.net" 加上 .scm 變成 "專案名稱.scm.azurewebsites.net" 不管是 App Service 或是 Web App 都可以進去看相關的資訊~

我有用到的有以下兩種：

1. Diagnostic dump，真的就是把整個有 log 的資料夾匯出，找到想看的
2. Log stream，可以即時的顯示 server 端的 log，只是開啟的速度真的慢到不行

### 發佈

當程式碼發佈到 Azure 時，其實會偷偷執行一些動作，若需要客製化就需要參考[官方文件][4]，執行 `kuduscript -y --node` 來產生相關配置檔，裡面就可以看到熟悉的指令，再按照需求去增修即可~

[1]: https://azure.microsoft.com/zh-tw/services/app-service/web/
[2]: https://azure.microsoft.com/zh-tw/services/app-service/
[3]: https://blog.miniasp.com/post/2015/05/04/Intro-Azure-Web-App-Kudu-engine.aspx
[4]: https://github.com/projectkudu/kudu/wiki/Custom-Deployment-Script
