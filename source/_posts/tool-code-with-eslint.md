---
layout: post
title: "ESLint 教學"
subtitle: "透過 ESLint 幫助我們控制程式碼品質"
date: 2017-02-21
author: "Lin Yen-Cheng"
catalog: true
tags:
  - JavaScript
  - Tools
---

### ESLint

ESLint 是一套強大的外掛，用來檢視程式碼的品質提示。當然使用的第一步就是在編輯器中裝入，再來就是定義 linter 需要的配置檔，否則像是 Atom 中預設是沒有配置檔就不啟用的 XD

### Code style

厲害的地方在也可以套用 airbnb 的 coding style，在 airbnb 的 [Github page][1] 也有教你怎麼 config，不然每個人加入團隊前光看 coding style rule 就飽了 Orz

有了這個工具就可以快速的自動提示 XDDD 如果還沒全面導入 ES6 寫法，記得在 rules 裡先 disable 掉一些短時間還來不及改的寫法 Orz

- "off" 或 0-關閉規則
- "warn" 或 1-開啟規則，使用警告級別的錯誤：warn(不會導致程序退出)
- "error" 或 2-開啟規則，使用錯誤級別的錯誤：error(當被觸發的時候，程序會退出)

詳細可以看 [ESLint 網站][2]上的配置教學~ 比較特別的是也有支援 jsdoc 的提示，對於事情雜亂一堆文件還沒補，有時補東就忘了西的人來說，真的是個優秀的發明 Orz

### Config file

使用方式很簡單就是要定義一個 .eslintrc 檔案在根目錄，就可以開始歡樂的使用囉 XD

```json
{
  "extends": "airbnb",
  "env": {
    "browser": true,
    "node": true,
    "es6": true,
    "mocha": true
  },
  "rules": {
    "one-var": 0,
    "no-var": 0,
    "prefer-template": 0,
    "valid-jsdoc": [
      "error",
      {
        "requireReturn": true,
        "requireReturnType": true,
        "requireParamDescription": true,
        "requireReturnDescription": true
      }
    ],
    "require-jsdoc": [
      "error",
      {
        "require": {
          "FunctionDeclaration": true,
          "MethodDefinition": true,
          "ClassDeclaration": true
        }
      }
    ]
  }
}
```

[1]: https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb
[2]: http://eslint.cn/docs/user-guide/configuring
