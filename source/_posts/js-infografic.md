---
layout: post
title: "網站資訊視覺化"
subtitle: "相關應用與工具簡介"
date: 2017-03-16
author: "Lin Yen-Cheng"
catalog: false
tags:
  - JavaScript
---

### 資訊視覺化應用

最近開始在研究資訊視覺化相關的呈現，除了統計圖表外，還有地理資訊在地圖上的顯示方式，地圖視覺化方式大概是熱點圖、群聚圖、航線、圖層上色後疊合，其中有看到幾個比較有趣的應用：[竊盜][1]、[重度急診即時訊息][2]、[環境儀錶板(pm 2.5)][3]、一定要推一下的~~制服(X)~~[正妹(O)地圖][4]、或是簡單的[麥當當距離圖][5]~

### 工具簡介

那搭配的函式庫呢？目前有讀了以下三種:

- Chart.js
- D3.js
- C3.js

當然是從最熱門的 D3 開始看 XDDD 初步的感覺是，D3 適合較進階的使用者使用，網路上一句解釋說的不錯 :

> D3.js 的基礎不是在視覺化，而是資料與物件的結合

D3 將資料對應到 dom 上，再透過方便的介面來詳細的定義各式需要的效果，文件要閱讀會需要一定的基礎知識，隨意舉一個 Voronoi Diagram，另外還有像是 topojson 這個蠻常在範例中看到個格式，可能都是很多人沒聽過的名詞 Orz

相對於困難的 D3，C3 和 Chart.js 就相對親民許多，C3 較 D3 容易這是當然的，畢竟 C3 就是以 D3 為基礎並簡化寫法的 Library，C3 和 Chart.js 兩者都是出圖表的函式庫，像是常見的 Line chart, Bar chart, Pie chart 等，Chart 在 Github 星星頗多，搭配基本客製化的配置檔其實基礎操作也夠用了，兩者使用起來的方法也差異不大~

[1]: https://moeaegis.cartodb.com/viz/9e6ca95c-7326-11e5-9150-0e3a376473ab/embed_map
[2]: http://er.mohw.g0v.tw/#/dashboard/file/all.json
[3]: http://env.g0v.tw/air/
[4]: http://uniform.wingzero.tw/
[5]: http://www.datapointed.net/visualizations/maps/distance-to-nearest-mcdonalds-sept-2010/
