---
layout: post
title: "Script 載入方式"
subtitle: "三種避免阻塞腳本的載入方式 (async, defer, lazyload)"
date: 2019-08-30
author: "Lin Yen-Cheng"
catalog: true
tags:
  - HTML
  - JavaScript
---

### 簡介與問題

HTML5 之後 [`<script>` tag][1] 中有多兩個 attribute 可以使用，透過這兩個 attribute 我們可以非同步的去載入腳本，來加快網頁載入的速度。

### 三種方法 (async, defer, lazyload)

- async: 如果可以就非同步，不保證按照順序，若沒有相依性且拿來操作畫面，是最佳做法
- defer: 頁面載入後都執行完才按照順序執行，可用來加速網頁載入
- lazyload 用程式控制載入時機

[async 按照 MDN 文件][2] 說明在 inline 的狀況不會有效果，必須帶有 src 屬性。:

```html
<script async>
  var code;
</script>
```

```js
var script = document.createElement("script");
script.innerHTML = "code";
document.body.appendChild(script);
```

### 使用程式懶載入

Promise 的範例

```js
function newScript() {
  return new Promise((resolve, reject) => {
    const script = document.createElement("script");
    script.src = "./test.min.js";
    script.addEventListener("load", () => {
      resolve();
    });
    script.addEventListener("error", e => {
      reject(e);
    });
    document.body.appendChild(script);
  });
}
newScript()
  .then(() => {
    self.setState({ status: true });
  })
  .catch(() => {
    self.setState({ status: false });
  });
```

Google Tag Manager 的範例

```js
(function(a, b, c, d, e) {
  a[d] = a[d] || [];
  a[d].push({
    "gtm.start": new Date().getTime(),
    event: "gtm.js"
  });
  var f = b.getElementsByTagName(c)[0],
    g = b.createElement(c),
    h = "dataLayer" != d ? "&l=" + d : "";
  g.async = true;
  g.src = "https://www.googletagmanager.com/gtm.js?id=" + e + h;
  f.parentNode.insertBefore(g, f);
})(window, document, "script", "dataLayer", "GTM-XXXXXXX");
```

[1]: https://developer.mozilla.org/zh-TW/docs/Web/HTML/Element/script
[2]: https://developer.mozilla.org/zh-TW/docs/Games/Techniques/Async_scripts
