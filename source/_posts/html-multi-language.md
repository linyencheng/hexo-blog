---
layout: post
title: "RTL 簡介"
subtitle: "當網頁遇到由右到左的語言"
date: 2017-02-21
author: "Lin Yen-Cheng"
catalog: true
tags:
  - HTML
  - JavaScript
---

### Multi language

最近在實作多國語言的時候，本來有打算使用也用到一半的 i18n，後來卻發現波斯文的排版整個需要由右到左(驚~XD

當然解決方案可以選擇把 style 跟元件中的 object 做 binding，然後操縱 object 來正確顯示由右往左的網頁，但設計那邊用 Dreamweaver 切版後丟出來的 css 要拆開重寫光看就是個大工程，尤其是每次拿到都要先用工具檢查錯誤，還不只偶而會少個;或}的 Orz

所以最後選擇用工具檢查兼壓縮樣式表，實作兩個排版及語言不同的多國語系網頁，貌似是短時間內最快也最方便的做法...

### RTL

讓文字由右至左，建議 html 或 body Tag 中，之後底下其他的直接繼承就不用再定 XD

以前做過的相關網站:
[Saipa Website][saipa website]、[Kia Website][kia website]、[Hyundai Website][hyundai website]

看是要寫在哪個 tag 裡都可以像是 `<html dir="rtl">`，不過 php send mail 的 html Body 裡面不能用`＂`，所以可能要寫成這樣 `body dir=RTL`，原因未知，不過只有不到一周的 php 經驗，也只能這樣先解了 Orz

最後把各種的相關的 left 改成 right，就大功告成一部分了 XD

```css
.left {
  float: left;
}
.right {
  float: right;
}
```

### State

另一個重要的就是資料邏輯，要在切換後正確顯示網頁：

1. 參數要正確
2. 狀態要正確

但當網頁做切換時，問題就來了...由於最後是選擇時做兩份，那兩支不同的程式切換時如何溝通目前的狀態...

目前這個專案大多只是改變 component 自身狀態，跨 component 的狀態共用大概只有是否登入 Orz，所以還不想使用太複雜的狀態管理 0.0 不過像是 Vuex, Redux 的文件都寫的很棒，蠻值得一讀~

目前用的方法是有點偷雞，藉由觀察 router 目前的 path，path 就可以知道自己在哪裏也可以放參數，外加瀏覽器本來就有 sessionstorage，交叉使用後勉強也算是把 "狀態" 拉到共用的地方也可以比對了 Orz

舉自動登入或登出來說:

1. 當我們進到某個 path
2. 判斷此 path 是否需要 login
3. 利用 sessionstorage 中是否存在某個值來確認登入狀態
4. 之後就是更改 router 的 path 來切換要顯示的元件

[saipa website]: http://saipa.polstargps.com/
[kia website]: http://kia.polstargps.com/
[hyundai website]: http://hyundai.polstargps.com
