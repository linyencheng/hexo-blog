---
layout: post
title: "React.js 專案架構"
subtitle: "從 React.js 的角度來看專案架構"
date: 2019-09-08
author: "Lin Yen-Cheng"
catalog: true
tags:
  - JavaScript
  - React.js
---

### React 簡介

React 是 component-based 的前端 UI 函式庫，通常會配合其他函式庫使用:

- react-router: 處理 SPA 的路由
- redux: 做統一的狀態管理，協助元件之間的溝通
- redux-thunk or redux-saga: 處理 AJAX 的 side effect

### Redux

Redux 主要就是做為元件之間需要溝通工具，有以下特性:

- 單向資料流
- 公共的狀態儲存 (Store)

專案架構也會受到這種概念去分類:

- actions: 觸發狀態改變用的 function
- containers: 有連接 Store
- components: 沒有連接 Store
- reducers: 收到 action 後的資料邏輯

### Side Effect

平常不太會去特別注意的名詞，我們用這個來分類 function 或元件:

- Pure function or UI 元件
  - 每次送進去固定的輸入，出來就會是固定的結果
  - 相同的 props, 產生相同的 component，會放在 components 資料夾
  - 不寫商業邏輯
  - 不加入有 side effect 的 code
- 有 side effect 的 function or 元件
  - 因為與狀態相關所以就會放在 containers 資料夾
  - AJAX，相關程式在 react hooks 中就會放在 useEffect，class component 就會放在 componentDidMount
  - AJAX 跟 redux，side effect 交給 redux-thunk 或是 redux-saga 去管理

最近開始接觸單元測試後，也深刻感覺到 side effect 越少，測試也越好寫，所以我想如果刻意去寫出好測試的元件，最後也會讓專案的結構變得更好偵錯與維護。

### 專案架構

以[最近弄的小專案](https://github.com/LinYenCheng/heros)當例子，如果有加上 redux 的話，專案架構大概會長成下面的樣子，在比較簡單的專案中可以更簡化。

```bash
├── src/
│  ├── containers/              # containers 放置與 Redux 連接的相關元件，單元測試檔案為 `元件名稱.test.js`
│  ├── components/              # components 放置相關元件，單元測試檔案為 `元件名稱.test.js`
│  │  └── Root.js               # 路由根目錄
│  ├── hooks/                   # hooks 相關
│  ├── middleware/              # 資料處理相關
│  │  ├── API.js                # axios 的 instance
│  │  └── redux-api.js          # redux-api
│  ├── styles/                  # 樣式檔們
│  ├── index.js                 # 程式入口
│  ├── serviceWorker.js
│  └── setupTests.js            # 測試相關設定
├── .eslintrc                   # ESLint 設定檔
├── .prettierrc                 # prettierrc 工具設定
├── .travis.yml                 # 持續發佈工具
├── package-lock.json
├── package.json
└── README.md
```

### 常用第三方工具

- classnames: 讓我們更方便的使用 JavaScript 去套入條件動態的操作 class
- gh-pages: 發佈到 github pages 的工具
- humps: 將收到的資料格式轉成駝峰式
- node-sass: 讓專案能夠使用 scss
- eslint-\*, lint-staged, husky, prettier: coding style 交給編輯器處理

### 簡化架構的函式庫與框架

對於專案或是後端收到的資料，若沒有太多需要客製化的部分，可用底下函式庫與框架簡化:

- [redux-api][redux-api]，將常用的寫法封裝
  - 簡化架構
  - 使用配置檔取代 redux 的專案中沒特殊處理的 action, reducers, API 的 CRUD
  - 減少寫太多重複的程式碼，由函式庫的配置檔取代
- axios，Promise based HTTP client for the browser and node.js
  - 前後端共構
  - 使用客製化 instance 的方式可以將收跟送的邏輯統一實作，減少重複的程式碼
- Next.js，是一套以 React.js 生態系打造出來的完整的框架，幫我們配置了
  - AMP
  - 路由
  - CSS-in-JS
  - 靜態頁面輸出
  - 伺服器渲染

[redux-api]: https://linyencheng.github.io/2018/08/20/react-redux-api/
