---
layout: post
title: "Google Trend & Analytics 簡介"
subtitle: "從 Google Alert, Google Trend, Google Analytics 來看關鍵字的規劃與分析"
date: 2018-11-24
author: "Lin Yen-Cheng"
catalog: true
tags:
  - Marketing
  - Tools
---

### 關鍵字規劃

- google alert: 針對特定主題建立，這樣當 Google 搜尋中出現與該主題相關的新結果時，就會收到通知。
- google trend: 可以看到台灣地區在凌晨 2~4 點時，搜尋嬰兒哭的比例到達顛峰
  ![cry](cry.jpg)
- 20 歲、25 歲、30 歲的自動完成字串有所不同(到底多在意長高???)
  ![20](20.jpg)
- 搜尋字詞報表：觀察消費者搜尋什麼
- 競爭對手用詞

人通常只會注意與自己相關的事物，所以商品有機會曝光的時候，要放置有關係的東西，Ex:正在春天階段的青少年，該推薦的東西就不會是慢跑鞋，就像是 2018 選舉的搜尋次數絕對被 7-11 優惠卡娜赫拉海放。 不過，舉個比較有趣的觀察，大選過後搜尋移民的趨勢:

![vote](vote.png)

所以先找到人在哪裡，人改變需求改變，文案就要改變，不同的人適合不同的打法。首先按照常態的經驗去提出一個假設:下關鍵字越精確的使用者會越有購買的可能。

那我們就可以針對關鍵字依照這個原則去處理，圖片名稱也很重要，盡量使用有意義的圖片名稱，譬如 "父親節特惠\_桁架結構避震慢跑鞋.png" 就會比 101-2-99.png 好很多。如果是跟我思維邏輯比較像的女孩子，買衣服下關鍵字可能? 會像上次講座中提到的 "甜美 雪紡紗 洋裝 過膝"，是真的會很精確 xddd 發現這樣的目標對象後，如果搭配農民曆外加發獎金的時間，其實又可以推個 "婚禮洋裝必搭外套"，大概很高機率讓人想剁手。

還有另外一個免費的[關鍵字工具][1]，已經幫我們做好好幾個平台的搜尋分析，整個也是推推。

一些小統計:

- 目前 50 ～ 70% 手機搜尋
- 建議搭配三則文案
- 如果需要拍攝影片的話，影片建議是 120 秒的影片，不過依照各個國情不同會有些許變化。

### Google Analytics (分析)

可以看出目標對象的相關統計，還可以搜尋網路上有先做好的樣板，並加入到自訂相關報表，就可以輕鬆顯示很多訊息了，像是:

- 時段及瀏覽量
- 使用者所在區域
- 是從 google 搜尋進來或是直接瀏覽
- 回訪率
- 使用的瀏覽器或是作業系統

我最近做了三次小實驗，因為我有將部落格連結放在 104 的自傳中，更新履歷後的隔天活耀的使用者就會明顯增加，還蠻有趣的 xddd 而且參加面試的當天，點擊量明顯也是平常的兩倍以上。

![interview](/images/interview.png)

### 前端網頁設計

首先當然是語意化，也就是 html 中 `<header>`, `<footer>`, `<nav>`, `<article>` ...等等，都按照相關標準去寫，再來就是按照標準置入[結構化資料(Microdata)][2]，讓機器能更快的讀懂我們的網頁，當然也不要忘了，關鍵字都要記得放在網頁裡，標題段落中都可以出現。

[1]: https://keywordtool.io/
[2]: https://developers.google.com/search/docs/guides/intro-structured-data
