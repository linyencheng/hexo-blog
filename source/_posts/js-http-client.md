---
layout: post
title: "Ajax 非同步請求整理"
subtitle: "幾個非同步發 Ajax 的方法"
date: 2017-04-20
author: "Lin Yen-Cheng"
catalog: true
tags:
  - JavaScript
---

## AJAX

先提醒一個重點，因為 javascript 本身是單線程的語言，這意味者一次只能做一件事~

在發送請求到伺服器時會需要等待回覆，如果這時伺服器一直沒有回應，那麼在單線程的情況下其他的任務就會被卡死了，因此也才有了非同步請求~

W3C 標準的非同步請求是利用 XMLHttpRequest 寫法，優點就是簡單直觀所有相關操作都從這個物件去取得，缺點大概就是結構會不漂亮，所以才有後來的 jQuery AJAX 以原生的標準為基礎，封裝後讓寫法更好的一種實作

## Promise 與狀態

已經加入 JS 也實作在各大瀏覽器裡了，目前 Chrome 32, Opera 19, Firefox 29, Safari 8 & Microsoft Edge 都有了，如果真的需要支援 IE 那可能需要 polyfill 一下 XD 比起原來的 XMLHttpRequest 會較推薦這種，Promise 建立之後，要不成功要不失敗，因為實體化的方式比較特別，所以一般會包在函式裡

```javascript
function asyncFunction(value) {
  return new Promise(function(resolve, reject) {
    if (value) resolve(value);
    // 已實現，成功
    else reject(reason); // 有錯誤，已拒絕，失敗
  });
}
```

然後主要就是使用 then 及丟進去的 callback 去取值出來，這裡的 callback 就都是非同步執行的喔!!!

```javascript
asyncFunction("test").then(function(datums) {
  console.log(datums);
});
```

定義可以看[這個網頁][1]，另外有個 Async/Await 還沒用過，似乎是下一代的解決方案，把 then 取代掉了，改成加上 Async/Await 的形容詞之後，就可以完全按照同步的方式去寫，看起來還蠻厲害的 Orz

## Fetch

AJAX 的新版本，詳情可看[MDN 的簡介][3]，看寫法的話就知道是已經基於 promise 來設計的，目前還沒有完全被各家瀏覽器實作，要使用的話需要 polyfill 一下，其中要注意的是預設 fetch 不會收或送 cookie 必須設定 `credentials` 這個參數才可以。

```javascript
fetch("url", { method: "get" })
  .then(function(response) {
    //處理 response
  })
  .catch(function(err) {
    // Error :(
  });
```

## Axios

[axios](https://github.com/axios/axios) 除了整合上面的功能外，還把一些常用的設定或是可能需要的防呆實作都整合進來，Vue.js 的文件也有介紹使用這套函式庫，透過配置將 client 和[vue 的 instance 綁在一起][3]，可以直接透過`this.$http`來叫用。

[1]: https://promisesaplus.com/
[2]: https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
[3]: https://vuejs.org/v2/cookbook/adding-instance-properties.html#Real-World-Example-Replacing-Vue-Resource-with-Axios
