---
layout: post
title: "Webpack 教學"
subtitle: "是個強大也愈來越夯的一個工具來著"
date: 2017-01-08
author: "Lin Yen-Cheng"
catalog: true
header-img: "post-bg-js-module.jpg"
tags:
  - JavaScript
  - Tools
---

## Foreword

> Here comes Module!

2016 年是個混亂的一年，Follow 一整年下來的感覺，主要看起來有點像是跟污染源濃度一樣三分天下(誤)

![three](three.jpg)
<small class="img-hint">真三分天下</small>

> 1.Angular 2：
> Google 推出的第二代，想得到的東西都裝在裡面了 XD

> 2.React.js：
> Facebook 主導，React 只有 UI，通常會和管理狀態的 Redux 一起用。

> 3.Vue 2.0：
> 目前在阿里 Weex 團隊的大大尤雨溪設計，有猛猛的 Vue-loader，可以把 CSS 也關起來，這樣好像就可以免除自己寫 BEM?

看起來各家的趨勢就都是元件化，元件化顯而易見的好處就是好維護可重用，不約而同的，各家也都推薦使用 Webpack 這套工具，在網路上一篇關於[模組化載入][1] 的文章中介紹：

- 瀏覽器端主要是 AMD(Asynchronous Module Definition)，因為在瀏覽器使用時，遠端 CDN 載不下來會有問題，所以早期比較推薦這種非同步的 Orz

- 非瀏覽器的環境中，Node 採用的[CommonJS][3]，因為都在本機端所以同步起來沒啥問題

- [UMD(Universal Module Definition)][4]，像是曾經的經典函式庫 [jQuery][5] 可以看得出來就是運用 UMD 來封裝，綜合各家優點?

- 其實還有沒完全被實作的 ES6 支援的模組語法，原生寫法就可以達到相同效果 Orz

[1]: http://fireqqtw.logdown.com/posts/255789-amd-umd-commonjs "amd-umd-commonjs"
[2]: https://github.com/umdjs/umd "UMD"
[3]: http://wiki.commonjs.org/wiki/CommonJS "CommonJS"
[4]: https://github.com/cmdjs/specification/blob/master/draft/module.md "CMD"
[5]: https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js "jQuery"

## WebPack

不過在前年年底發現了 Webpack，開始使用之後就想：到底還要不要清楚了解這些複雜的規範？~~好像就不重要了 Orz~~ Webpack 除了是個厲害的 build tool 也是一個載入 JS 模組的工具，方便的幫你管理程式間複雜的相依性，提供 hot reload~~也就是程式的熱插拔~~，同時也有一些 loader 可以裝，裝上之後瞬間多了 JS 編譯器或是 CSS 預處理的功能，最後會包成一包 Bundle 輸出，還能裝一些最佳化的外掛，像是 UglifyJS，就是個方便好用又很猛的工具。

講了一堆介紹，結果使用 Webpack 後發現根本沒管那些同步不同步的概念~~(所以真的不重要嗎 Orz)~~，Webpack 其實也有辦法支援 ES6 的寫法，所以建議直上 XDDD

最後輸出成一整包後最佳化的檔案就可以直接用了。

使用方法：
{% codeblock lang:js %}
// 官網範例
// salute.js
var MySalute = "Hello";
module.exports = MySalute;

// world.js
var MySalute = require("./salute");
var Result = MySalute + "world!";
module.exports = Result;
{% endcodeblock %}

`exports`就負責輸出，`require`負責載進來。

想要使用的時候就

```js
var Result = require("./world");
```

會需要寫一個 webpack.config.js 的配置檔,entry 就是入口，要機器幫你 build 總要跟人家說從哪開始吧 XDDD 最後 output 的就是包成一包的檔案了，這個是官網範例只有多一個簡單的　 css loader，還可以依照需求裝自己的需要~

```js
module.exports = {
  entry: "./entry.js",
  output: {
    path: __dirname,
    filename: "bundle.js"
  },
  module: {
    loaders: [{ test: /\.css$/, loader: "style!css" }]
  }
};
```
