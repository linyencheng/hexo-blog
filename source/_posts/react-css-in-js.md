---
layout: post
title: "CSS in JS"
subtitle: "淺談 CSS in JS 與優缺點分析"
date: 2019-08-29
author: "Lin Yen-Cheng"
catalog: true
tags:
  - React.js
  - JavaScript
  - css
---

### 背景

近幾年，由於 component-based 的概念興起，jQuery 逐漸被 React、Vue 取代，以往我們都是將樣式檔分開寫，當元件開發成為顯學時，是否有更適合的寫法？

### 問題定義

當元件需要在其他專案使用時，在搬動上會增加許多不必要的困難，像是除了必須搬完相對應的樣式檔，然後還必須放在正確的位置中。

當我們用了其他的函式庫，像是在 Next.js 的專案最佳化時，樣式檔的引用順序就必須讓機器不會 confuse，但若是我們將樣式檔拆的太細，很可能就會發生在 code splitting 時機器無法判斷順序的問題。

### 優點與解決的問題

CSS in JS 是減少麻煩的一個好概念，直接把該寫的樣式一起寫在元件中，有使用過也蠻推薦的像是 styled-components、styled-jsx ( Next.js 內建並維護，較適合有伺服器渲染的專案)。

- 解決 code spliting 可能出現的順序問題
- 命名互相覆蓋的問題，在撰寫樣式檔時也可以用 BEM 的命名規則來避開
- 維護時不需要再去找到底被哪個樣式檔影響
- 自動幫樣式加上瀏覽器的前綴，樣式檔就要再加上 postCSS 幫忙處理

### 缺點

- 切換主視覺要多用一個 `<ThemeProvider>`，傳入的 Props 差異太大時有點麻煩
- RWD 不像 bootstrap 有格線系統那樣使用方便，建議也可以只導入格線系統混合使用
- 會比較難做效果跟做覆蓋，不同狀態需要不同樣式時要再封裝一層
- 語法的高亮、自動完成、防呆較不完整
- 少了可以共用樣式的好處，像是 `@extend` 或 mixin 這樣的寫法，需把元件切的夠小，才會有一定程度的共用性

### styled-components

```js
import styled from "styled-components";

function Sample() {
  const StyledButton = styled.button`
    color: #ffffff;
    font-size: 20px;
    border-radius: 5px;
  `;
  return <StyledButton type="button">按鈕</StyledButton>;
}
```

### styled-jsx

```js
function Sample() {
  return (
    <>
      <style jsx>
        {`
          button {
            color: #ffffff;
            font-size: 20px;
            border-radius: 5px;
          }
        `}
      </style>
      <button type="button">按鈕</button>
    </>
  );
}
```
