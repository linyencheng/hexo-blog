---
layout: post
title: "無障礙網頁設計 (a11y)"
subtitle: "用鍵盤也可以使用的無障礙網頁"
date: 2018-02-04
author: "Lin Yen-Cheng"
catalog: true
tags:
  - HTML
  - CSS
---

### 無障礙網頁簡介

無障礙網頁跟一般網頁的差異，主要在協助視覺或是聽覺障礙者進行瀏覽。

1. 可以使用滑鼠以外的工具來進行網站瀏覽
2. 可以使用螢幕閱讀器進行閱讀

### Accessibility

最近才開始進入 react 16 版，發現說明文件也改版了！除了已經在官方有建議的 code-split 方法，竟然還有[a11y 無障礙的網頁設計建議][1]，我們還可以安裝 `eslint-plugin-jsx-a11y`，這樣 ESLint 就會提示我們 JSX 中有哪些沒有寫好的部分。

首先剛開始的 aria-label 或是進階一點的 aria-labelledby 都是為了增加螢幕閱讀器的辨識度，再來就是需要 focus 的 outline 效果，這樣在使用鍵盤操作的時候，能夠輕鬆的看出我們目前的所在位置，移動上主要是使用 `tab` 或是 `shift + tab` 來選擇 `<input> <a> <button>`，另外比較特別的是 [accesskey][2] 的屬性可以方便我們快速的利用鍵盤跳到某個區塊，政府網頁通常會在上面設置[導盲磚 :::][3] 來協助，藉由搭配 title 屬性文字方便語音合成器告知使用者，是在網頁中的哪一個區塊。

### 設計部分與檢測工具

其他就是比較偏設計，像是顏色的對比度，或是圖片要有替代文字，這些比較細節的設計可以使用 chrome 的外掛 [wave-evaluation-tool][4] 來幫助，如果需要得到有跟沒有一樣?的標章認可，那就需要下載政府的[檢測軟體][5]來檢測。

[1]: https://reactjs.org/docs/accessibility.html
[2]: https://developer.mozilla.org/zh-TW/docs/Web/HTML/Global_attributes/accesskey
[3]: https://www.handicap-free.nat.gov.tw/Questions/Detail/82?Category=33
[4]: https://chrome.google.com/webstore/detail/wave-evaluation-tool/jbbplnpkjmmeebjpijfedlgcdilocofh
[5]: https://www.handicap-free.nat.gov.tw/Download
