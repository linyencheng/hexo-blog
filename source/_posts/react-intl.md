---
layout: post
title: "React 多國語言簡介(使用 react-intl)"
subtitle: "使用 Yahoo react-intl"
date: 2017-09-21
author: "Lin Yen-Cheng"
catalog: true
tags:
  - React.js
  - JavaScript
---

### 安裝

Install it: npm install --save react-intl

### 專案配置

先加入`addLocaleData`各語系配置檔，呆丸專用繁體中文 zh-Hant-TW 已包含在 zh 中可以直接使用，然後透過`IntlProvider`注入相關環境及設定到我們的 App 中，其中像是現在要顯示的語系(locale)及翻譯黨(messages)，然後就可以透過更改 local 及 message 達到語言切換的目的~

```js
import { addLocaleData } from "react-intl";
import en from "react-intl/locale-data/en";
import zh from "react-intl/locale-data/zh";
import es from "react-intl/locale-data/es";

addLocaleData([...en, ...zh, ...es]);

<IntlProvider locale={localeProp} key={localeProp} messages={messagesProp}>
  <App />
</IntlProvider>;
```

### 使用

假設雙語的話，就是三位一體的概念，首先必須把專案裏需要雙語的地方改成特定元件或是變數,接著配合剛才的元件或變數中的`id`撰寫翻譯檔,然後透過改變剛剛提到的 locale, message 去對應切換相關語言翻譯檔~

一般顯示

```js
<FormattedMessage id="App.hello" />
```

當成變數使用，注入後使用

```js
const App = props => {
  const { intl } = props;
  const strHello = intl.formatMessage({ id: "App.hello" });
};
injectIntl(App);
```

英文語言

```json
{
  "App.hello": "hello"
}
```

中文語言

```json
{
  "App.hello": "哈囉"
}
```

### 小建議

完全建議在專案一開始就加入多國語言的配置，這樣才不會到後來各種麻煩 QQ 底下 Demo 了一個交換語言用的 Container，透過每次改變語言去改變狀態並傳入 `IntlProvide` 中，當然也可以考慮用 HOC 的方式去包裝~

```js
class HotSwappingIntlProvider extends React.Component {
  constructor(props) {
    super(props);
    const { initialLocale: locale, initialMessages: messages } = props;
    this.state = { locale, messages };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(locale) {
    let messages = enJson;
    if (locale !== "en") {
      messages = zhJson;
    }

    this.setState({
      locale: locale,
      messages: messages
    });
  }

  render() {
    return (
      <IntlProvider
        locale={this.state.locale}
        key={this.state.locale}
        messages={this.state.messages}
      >
        {this.props.children}
      </IntlProvider>
    );
  }
}
```

[1]: https://github.com/yahoo/react-intl/issues/213
[2]: https://github.com/yahoo/babel-plugin-react-intl
