---
layout: post
title: "Python Hypothesis Testing (假設檢定)"
subtitle: "使用 Python 快速入門假設檢定、資訊視覺化"
date: 2019-08-01
author: "Lin Yen-Cheng"
catalog: true
tags:
  - Tools
  - Back-End
---

> 首先感謝 [Tainan.py][tainanpy] 舉辦這次很棒的年會

如果大家有興趣的話可以關注一下他們的 [Meetup 頁面][tainanpy]、[南部社群的臉書社團][tainanpyfacebook]。

### 環境安裝及設定

首先當然是安裝 python 本人，如果需要在 cmd 中可以執行 `python hello.py`，需要先在環境變數中增加相關路徑。

https://www.python.org/downloads/

另外 python 預設是將相關函式庫安裝在 global 的環境中，在開發的時候可能會常常需要做實驗，所以我們會需要一個實驗的環境，那就是安裝虛擬環境管理工具 Anacoda，在 node.js 中就是一個避免每次都執行 `npm -g` 的概念。

https://www.anaconda.com/distribution/

```bash
pip3 install scipy  # 沒有虛擬環境
conda install scipy # 使用虛擬環境
```

conda 的常用指令

```bash
conda list
conda env list
conda create --name TESTENV python=3.6
conda activate TESTENV
conda deactivate
conda install scipy
conda list
```

### 資料集簡介

這次很幸運的可以在社群聚會聽到 Mosky 大大的開示 ([投影片在這裡][mosky])，了解一些檢定在電商上可能的應用。不過由於商業機密的關係，這次講解的範例，主要以 [statsmodels][statsmodels] 中的範例資料集 [fair][fair] 來講解，這是一份關於外遇的統計資料， 資料筆數 6367 筆，檢定主要希望可以看到一些有趣的訊息，舉例來說像是職業會不會影響婚外情。

### 假設檢定簡介

首先從我們的資料看起，在統計上分析可以分成兩種案例：

- 有母數：分佈相同可以代表樣本，那就可以從平均數來看一些假設
- 無母數：如果不確定分佈是否相同- 基本上就是中位數，像是台灣的薪水分佈

在做假設檢定的時候會有兩種假設

- null hypothesis: 是我們要推翻的假設，期望值相減等於零，可以直接建立模型
- alternative hypothesis: 我們想證明但可能不好觀察，透過否證法證明(推翻 null hypothesis)

其中我們會用到 p-value，簡單來說 p-value 就是觀察抽樣出來的機率密度，如果越低代表不可能，主要使用這個來判斷假設是否正確，當然可以接受的範圍要看資料內容，不能跟新聞一樣斷章取義 QQ

應用上舉例來說，在某個情境下，我們會定義一個模型，然後期待把資料丟進去算出機率。透過這樣的機率來判斷是否要相信我們的假設，通常這個要依據我們的商品而定，像是筆電負評率 1% 燈泡可能可以到 10%。

### 檢定可解決的問題

適合回答是否有改變的問題，像是男生和女生平均購物金額是否有差異，或是購買皮卡丘玩偶在不同年齡的男女比例是否有差異。另外一個問題是 sample size 要怎麼決定？假設今天只是猜男女這種五五波問題，sample size 假設需要一千，但如果需要猜測平均轉換率 2%，可能相對要十萬。

- 平均值問題：welche t-test
- 比例問題：卡方檢定
- 找出兩組不同的機率: power analysis

### 好用工具

在 python 中有相當多的好用工具：

- scipy 中提供相當多的數學計算公式，這次使用較多的為 stats
- pandas 可以從異質資料來源讀取檔案內容，並將資料放入 DataFrame 中，data frame 相當於 execl 中一個 sheet，DataFrame [] 相當於 filter 或是 sql 中的 select
- seaborn 講者大力推薦 python 中畫圖的一個函式庫，將統計數據視覺化，像是 pointplot、countplot
- matplotlib.pyplot 畫完圖如果要顯示可以用這個工具

### 實作

按照教學上的說明，我們要先安裝相關函式庫，有以下兩種方法:

```bash
pip3 install jupyter numpy scipy sympy matplotlib ipython pandas seaborn statsmodels scikit-learn
conda install jupyter numpy scipy sympy matplotlib ipython pandas seaborn statsmodels scikit-learn
```

直接進入範例，範例中將看婚姻滿意度對外遇的影響，這裡定義數字小於 2 我們當作不滿意。

```python

import scipy as sp
import statsmodels.api as sm
import seaborn as sns
from IPython.display import display
import matplotlib.pyplot as plt

print(sm.datasets.fair.SOURCE,
      sm.datasets.fair.NOTE)

# -> Pandas's Dataframe
df_fair = sm.datasets.fair.load_pandas().data
df = df_fair
# 2: poor
# 3: fair
df = df.assign(poor_marriage_yn=(df.rate_marriage <= 2))
df_fair_11 = df

df = df_fair_11
display(df
        .groupby('poor_marriage_yn')
        .affairs
        .describe())
a = df[df.poor_marriage_yn].affairs
b = df[~df.poor_marriage_yn].affairs
# ttest_ind(...) === Student's t-test
# ttest_ind(..., equal_var=False) === Welch's t-test
print('p-value:',
      sp.stats.ttest_ind(a, b, equal_var=False)[1])

df = df_fair_11
sns.pointplot(x=df.poor_marriage_yn,
              y=df.affairs)
# show result
plt.show()

```

結果，最後真的可以看出有明顯差異，所以是否該開始照顧另一半的滿意度了！

![hypothesis-testing](/images/hypothesis-testing.png)

[tainanpy]: https://www.meetup.com/Tainan-py-Python-Tainan-User-Group/
[tainanpyfacebook]: https://www.facebook.com/groups/mosut/
[statsmodels]: https://github.com/statsmodels/statsmodels
[pandas]: https://oranwind.org/python-pandas-ji-chu-jiao-xue/
[fair]: https://github.com/statsmodels/statsmodels/blob/master/statsmodels/datasets/fair/fair.csv
[mosky]: https://speakerdeck.com/mosky/hypothesis-testing-with-python
