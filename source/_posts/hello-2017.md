---
layout: post
title: '總覺得一定要來一發...... "Hello World"XDDD'
subtitle: "Github 上的部落格耶 哇哈哈哈"
date: 2017-01-07 12:00:00
author: "Lin Yen-Cheng"
header-img: "post-bg-2015.jpg"
tags:
  - Tools
---

> 這弄弄那弄弄的，第一個 Blog 就打開了。

[跳過碎碎念 XD](#build)

去年年底，覺得好像差不多可以準備換工作了，就加加減減看了一些職缺，也發現有些職缺甚至開在 Github 的 [issues][1]裡，於是就開了一個帳號。

加上也發現許多工作面試有這個需求，所以就搞一個吧。也因為之前公司是 SVN 所以沒用過 Git，這次先練習上傳了一些平常的練習。

之前也常常逛 Github，發現有些人會利用這個空間來開部落格，於是乎稍微研究了一下，就決定開始動手做一個 XDDD

![me](me.jpg)
<small class="img-hint">放個今年第一張被拍的帥照好像是必須的 XDDD</small>

[1]: https://github.com/f2etw/jobs/issues "issues"

---

<p id = "build"></p>

## 簡介

其實 Github 本來就有提供個人放置解說網頁的功能，利用 [GitHub Pages](https://pages.github.com/) + [Jekyll](http://jekyllrb.com/) 就可以快速產生。

但樣式就稍微單調一點，上網查詢了以後,發現已經有框架可用了!!!
這次主要是利用 hexo 這套框架。

> Hexo: A fast, simple & powerful blog framework

而且還支援 **Markdown**，使用上其實真的蠻簡單的，也沒什麼大問題，搭配官網裡其他大大的樣式，簡單的就可以開始進入寫部落格的階段了 XDDD

而且也有提供把程式碼嵌在裡面的功能,根本專門給人寫筆記用的 XDDD

```javascript
function autoDetect() {
  console.log("ye, it is written in JavaScript!");
}
```

—— 2017.01
