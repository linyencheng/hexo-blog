---
layout: post
title: "VS Code Extensions 推薦"
subtitle: "前端工程師常用的 VS Extensions 整理 (React)"
date: 2019-06-25
author: "Lin Yen-Cheng"
catalog: true
tags:
  - Tools
---

### 前言

在幾年前，前端的編輯器還沒有那麼方便，一直到 node.js 出現以後，很多事情開始出現了變化，electron 結合了 node.js 還有 chromium，讓 js 也可以開發桌面端的應用程式，所以也就代表著，原本的文字編輯器也可以執行 js 了，這樣一來，一些基本的錯誤就可以先在編輯器中被發現，而不用每次都開啟瀏覽器才發現問題。

在 Atom 驚為天人的出現之後，微軟也推出了 VS code，相關外掛在這幾年也越來越完整。做為一個前端攻城獅，在使用了 VS 一陣子後，也有一些想推薦給大家。

### 自動完成篇

- Auto Close Tag: 寫網頁或是 jsx 的時候很方便，可以少打很多東西。
- Path Intellisense: 引入檔案的時候，如果資料都有照邏輯擺，那這個就能加速找檔案過程。
- SCSS IntelliSense: 協助寫一些像是 `@include` 的語法。
- Reactjs code snippets: 較新的語法可能未支援，優點打 rccp 就可以自動完成元件架構。
- HTML Snippets: 有時候在切單純一頁 html 的時候好用，可以少做很多事情。

### 加強防呆

- Code Spell Checker: 常常英文拼錯但人腦還是可以辨識，所以改錯字就只能靠外掛了。
- [ESLint][eslint]: 若搭配安裝 airbnb 提供的相關配置，就能夠把部分的 Coding style 還有協助不好寫法的工作交給 airbnb 團隊了。
- Sass Lint
- markdownlint

### 排版、Syntax highlighting

- Sass
- DotENV: 環境變數
- Babel JavaScript: 只要是寫比較流行的框架或函式庫大概都無法避開。
- vscode-styled-jsx: React 開發必備。
- TODO Highlight: 有寫過 Java 的話應該很熟悉這就是 Eclipse 的任務標籤。
- Color Highlight: 色碼會幫我們上色。
- Beautify css/sass/scss/less
- Stylesheet Formatter
- JSON Tools

### 伺服器、後端相關

- Apache Conf
- Apache Conf Snippets
- nginx.conf
- nginx-formatter
- Swagger Viewer
- YAML

[eslint]: https://linyencheng.github.io/2017/02/21/tool-code-with-eslint/
