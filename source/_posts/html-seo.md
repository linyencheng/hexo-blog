---
layout: post
title: "SEO 優化"
subtitle: "HTML語意化, Meta data , Microdata(結構化資料), 網站索引檔(sitemap.xml)"
date: 2018-09-29
author: "Lin Yen-Cheng"
catalog: true
tags:
  - Marketing
  - HTML
---

### HTML 語意化

從前端攻城獅的角度，來看有什麼地方是可以加強的，其中重要的地方在 html 的語意化，語意化帶來的好處一方面是方便維護，二來當程式交接的時候，只要是理解標準的人很快就可以了解網頁結構，另一方面在 [html 5][1] 訂出標準後， `<section>, <article>, <nav>, <header>, <footer> , <aside>` 更直觀的標籤出現，這樣機器或是人也就可以更容易了解網頁的架構。

### 中繼資料 (meta data)

[中繼資料 (meta data)][2] 比較重要的是提供各平台的資訊，主要用於跟各平台溝通以及顯示資訊，舉例來說像是可以跟搜尋引擎說不要索引這個網站、或是跟 Facebook 說分享時顯示的圖片是這張，這樣在在各平台時，才能夠顯示正確且適當的資訊，Facebook 也提供了分享[偵錯工具][3]來防呆。

### Microdata

再來是 Microdata [結構化資料][4]，我目前還沒有真的實作過，但看起來如果想要讓 google 的搜尋結果出現厲害的卡片或表格，甚至是其他未來可能出現更酷炫的結果，在網頁商品設計的同時，這個看來是必須認真去規劃的，實作之後也別忘了使用 [google 提供測試工具][5]測試一下。

### Sitemap.xml

最後是定義 Sitemap.xml ，各種語言應該都有相對應的工具能夠自動產生這個配置檔，如果想要手動提交的話，可以透過 google search console 去提交或是停止顯示某些頁面，用途是告訴搜尋引擎目前網頁有哪些路徑需要給機器去閱讀和索引，下面的例子就是定義更新的頻率以及重要程度。

```xml
<url>
    <loc>https://sample.com/samples</loc>
    <lastmod>2018-09-28</lastmod>
    <changefreq>monthly</changefreq>
    <priority>0.9</priority>
</url>
```

### Google Search Console 成效分析

當網站實際運行一陣子後，我們可以透過 Google Search Console 成效分析來看使用者常搜尋的關鍵字，以及個體連結與關鍵字的曝光次數關係。

### Google Analytics

這裡主要可以看到網站中總體上時間跟使用者的關係，可以更快的了解到我們的目標對象，確認了目標對象後也才容易進行關鍵字的優化。

[1]: https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/HTML5
[2]: https://support.google.com/webmasters/answer/79812?hl=zh-Hant
[3]: https://developers.facebook.com/tools/debug/sharing/
[4]: https://developers.google.com/search/docs/guides/intro-structured-data
[5]: https://search.google.com/structured-data/testing-tool
