---
layout: post
title: "Java Web 教學"
subtitle: "第一次寫 Java Web 就上手"
date: 2018-04-05
author: "Lin Yen-Cheng"
catalog: true
tags:
  - Back-End
---

### 相關資源

As Title，分成前後端及資料庫部分，需打通的關節分三個方面。
前端網頁(網址) <---Q1---> RESTful Web Service <---Q2---> Web 伺服器 <---Q3---> 實體資料庫
Q1：如何寫出前端網頁可使用的 RESTful Web Service？
Q2：如何將程式發佈至伺服器供前端存取？要發佈在何種伺服器上？
Q3：如何讓撰寫的 RESTful Web Service 存取實體資料庫？

相關知識如下
Coding Conventions，推薦撰寫方式及風格上的統一，用意在於方便閱讀及增進效能。
Java 後端 (http://www.oracle.com/technetwork/java/codeconventions-150003.pdf )
Java Script 前端 (http://www.w3schools.com/js/js_conventions.asp )

語法教學資源網站
Java (http://openhome.cc/Gossip/Java/ )
JavaScript (http://www.w3schools.com/js/default.asp )
JavaScript Practice (https://www.codecademy.com/tracks/javascript )

IDE (Integrated Development Environment) 選擇
我用這個 Eclipse (http://www.vogella.com/tutorials/Eclipse/article.html )
NetBeans (https://netbeans.org/kb/docs/java/quickstart.html )
Intellij idea (https://www.jetbrains.com/idea/ )

相關 Framework 學習
Jersey (https://jersey.java.net/documentation/latest/ )
Jersey (http://examples.javacodegeeks.com/category/enterprise-java/rest/jersey/ )

OpenSource 平台
前端 (http://codepen.io/ )

資料庫
MySQL (http://www.codedata.com.tw/database/mysql-tutorial-getting-started )

### 系統配置

斯斯有三種，感冒用~咳嗽用~鼻塞流鼻水用 XDDD
那 Java 也有三種，依照需求會選用不同版本，如下：
SE(Standard Edition)
ME(MicroEdition)
EE(Enterprise Edition)
介紹文連結（http://www.codedata.com.tw/book/java-basic/index.php?p=ch1-2 ）

開工前，請先下載
JDK (Java Development Kit)，從翻譯來看就是開發工具組，JDK 中就包含 JRE (Java Runtime Environment)，有 JRE Java 程式就可以執行了。
記得設環境變數，讓電腦找到工具組!!! 不然就!!! 什麼事都不會發生 XD

因為要寫 Web 應用，所以選擇 Java EE，下面是 EE 肚子裡裝 der 東西 XDDD

(請參考圖片: http://blog.arungupta.me/wp-content/uploads/2013/10/javaee7-pancake.png )

環境設置完之後就是選用 IDE 的時候惹，這篇附上 Eclipse 的相關快捷鍵，另外建議自動完成須更改一下設定，讓鍵入每一字的時候都可以出現。設定位置在 Window → Preferences → Java → Editor → Content Assist，Auto activation triggers for Java 為 IDE 遇到何種字元會自動啟動提示，請改為 .abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ(,；，就會遇到什麼字元都自動提示惹。

| 快捷鍵                 | 功能               |
| ---------------------- | ------------------ |
| Ctrl+Shift+T           | Opening a class    |
| Ctrl+Shift+R           | Opening a Resourse |
| F3 or ctrl+mouse click | go into a class    |
| Ctrl+O                 | Quick Outline      |
| Ctrl+H                 | Search menu        |
| Ctrl+J                 | Incremental Find   |
| Ctrl+Shift+/           | 全部收合           |
| Ctrl+\*                | 全部展開           |
| Ctrl+Shift+F           | 自動排版           |

### 創建一個 Java Web Appication

首先！建立 Dynamic Web Project。
選擇 Runtime Server，列表請看第 12 頁。
Tomcat (http://ftp.mirror.tw/pub/apache/tomcat/tomcat-8/v8.0.26/bin/apache-tomcat-8.0.26.exe )
Jetty Plug-in (http://run-jetty-run.googlecode.com/svn/trunk/updatesite )
JBoss (http://mkn939.blogspot.tw/2013/05/eclipsejboss-as-7-new-server.html )
可勾選自動生成 Web.xml ，或是自己打 Orz
目標！利用 Jersey 這個 Framework 實做功能並發佈到湯姆貓上， 未使用自動建構工具前，需自行下載 Library 並放進 WebContent/Web-INF/lib 資料夾裡，這是 War 檔中固定的架構勿更改。

實作！新增相關 class，並按照範例寫作，可依照這篇提供的範例 (http://www.vogella.com/tutorials/REST/article.html )，做到第六章即可 XD

完成之後我們就一次打通了 Q1 及 Q2 了 ╰(￣︶￣)╯

前端網頁(網址) <---Q1---> RESTful Web Service <---Q2---> Web 伺服器
A1：利用 Jersey 寫出 RESTful Web Service
A2：JBoss 可利用 Export War 檔，將檔案發佈至 Server

### 透過 Jersey RESTful Service 寫入資料庫

現在剩下最後一個 Q3 惹!!!!!!!!!!
Web 伺服器 <---Q3---> 實體資料庫
Q3：如何讓撰寫的 RESTful Web Service 存取實體資料庫？
A3：複習 Java 連接 MySQL 的練習，並整合進 RESTful Service 中。
按照以下飯粒：

```java
@Path("/test")
public class TestRestPractice {
   我是之前實作class test = new 我是之前實作class();

     @GET
    public String hello() {
    test.我是之前實作的功能();
     return "hello >////<";
    }
}
```

為了避免一些問題，可以參考廣為流傳的設計模式，像是 [Singleton Design][singleton design]。

設計模式，提供了大家程式架構的參考

在[這範例中][1]的 ArticleDao，就有用到 [Singleton][2] 的概念，就是單一個 Instance。Singleton 模式可以保證一個類別只有一個 Instance。

```java
public enum ArticleDao {
    instance;
    //利用HashMap當資料來源
    private Map<String, Article> contentProvider = new HashMap<>();

    private ArticleDao() {

    Article article = new Article("1", "Learn REST");
    article.setContent("Read http://www.vogella.com/tutorials/REST/article.html");
    contentProvider.put("1", article);
    article = new Article("2", "Do something");
    article.setContent("Read complete http://www.vogella.com");
    contentProvider.put("2", article);

    }

    public Map<String, Article> getModel() {
        return contentProvider;
    }
}
```

其他地方則透過 `ArticleDao.instance.getModel().values()` 來叫用內部資料。

[1]: http://www.vogella.com/tutorials/REST/article.html
[2]: http://charlesbc.blogspot.tw/2009/04/design-pattern-singleton.html
[singleton design]: https://en.wikipedia.org/wiki/Singleton_pattern
