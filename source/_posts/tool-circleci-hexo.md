---
layout: post
title: "Circle CI X Hexo Blog"
subtitle: "使用 Circle CI 自動化發佈 Hexo Blog"
date: 2019-8-12
author: "Lin Yen-Cheng"
catalog: true
tags:
  - Tools
---

### 問題定義

希望可以運用 Circle CI 做到自動化發佈 Hexo 建置的 Blog。

原來的流程是當我們寫好一篇新的文章後，需要自行執行以下指令來完成發佈，這次的目標就是要將這個工作交給 Circle CI 這個自動化工具。

1. `hexo clean`
2. `hexo generate`
3. `hexo deploy`

### Circle CI 注意事項

Circle CI 可以協助我們將 Github 上的專案進行持續整合，免費使用的話，每個月有 1,000 build minutes，又因為 hexo 本身有整合好發佈的 script，如果搭配 Circle CI 我們就可以讓文章發佈的動作自動化了，大概每次發佈都可以在 2 分鐘內結束。

Circle 是使用 YAML 檔進行設定的，所以首先需要在專案中跟目錄建立 `.circleci` 資料夾，並新增下面的 `config.yml` 到資料夾中。

```yaml
# Javascript Node CircleCI 2.0 configuration file
#
# Check https://circleci.com/docs/2.0/language-javascript/ for more details
#
version: 2
jobs:
  build:
    docker:
      # specify the version you desire here
      - image: circleci/node:10.16.2

      # Specify service dependencies here if necessary
      # CircleCI maintains a library of pre-built images
      # documented at https://circleci.com/docs/2.0/circleci-images/
      # - image: circleci/mongo:3.4.4

    working_directory: ~/repo

    steps:
      - checkout

      # Download and cache dependencies
      - restore_cache:
          keys:
            - v1-dependencies-{{ checksum "package.json" }}
            # fallback to using the latest cache if no exact match is found
            - v1-dependencies-

      - run: npm install

      - save_cache:
          paths:
            - node_modules
          key: v1-dependencies-{{ checksum "package.json" }}

      # run tests!
      - run: npm run clean
      - run: npm run generate
      - deploy:
          command: |
            git config --global user.email $GH_EMAIL
            git config --global user.name $GH_NAME
            npm run deploy
```

放了之後執行可能會發現有點問題，因為還有一些注意事項要記得：

1. 要去專案設定中加入 SSH key，並確認是擁有讀寫權限的，這樣自動發佈才會正常
2. 如果專案是開放的，個資可以放在 Circle CI 環境變數中，`$GH_EMAIL`、`$GH_NAME`
3. 因為全域環境中沒有安裝 hexo，所以才需要新增 npm script 去取代 hexo script

都確認之後再執行一次看看，如果亮綠燈的話就恭喜，以後寫完就只要 push 到 github 剩下的工作就交給 Circle CI。
