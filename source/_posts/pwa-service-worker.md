---
layout: post
title: "Service Worker 介紹"
subtitle: "實作快取及推播的一種解決方案"
date: 2017-07-22
author: "Lin Yen-Cheng"
catalog: true
tags:
  - PWA
  - JavaScript
---

### Service Worker

最近因為需要實作推播訊息，所以會需要看到 service worker，還記得第一次看到這個名詞是因為看 [Progressive Web App][1]，這裡比較新的觀念是可以幫助 web app 在網路較慢或是沒有連線時去使用快取資料。

另外一個特性是 service worker 可以在頁面沒開啟時背景執行，由於可以背景執行，所以接受推播的任務也可以在這裡進行撰寫，需要注意的是推播分成兩部分實作，一部分是通知，一部分是接受推播，後端各家的概念上其實都是先針對 topic (可以想像成頻道)進行訂閱，然後撰寫接收事件，然後觸發通知。

底下 Demo 了第一次先將圖片 icon 快取，第二次如果發現有使用到 dog 時則用 icon 取代，可以明顯看到有幾個階段`install`, `activate`, `fetch`,其實就是透過實作這幾個事件的觸發行為來達到我們的目的。

### 練習

```js
self.addEventListener("install", event => {
  console.log("installing…");

  // cache a icon
  event.waitUntil(
    caches.open("v1").then(cache => cache.add("images/icon.png"))
  );
});

self.addEventListener("activate", event => {
  console.log("ready to handle fetches!");
});

self.addEventListener("fetch", event => {
  const url = new URL(event.request.url);

  // serve the horse SVG from the cache if the request is
  // same-origin and the path is '/dog.svg'
  if (url.origin == location.origin && url.pathname == "/images/dog.jpg") {
    console.log(url);
    event.respondWith(caches.match("images/icon.png"));
  }
});
```

[1]: https://developers.google.com/web/fundamentals/getting-started/codelabs/your-first-pwapp/?hl=zh-tw
