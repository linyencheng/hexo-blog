---
layout: post
title: "當機器人遇上行銷管理"
subtitle: "我們該如何讓 Marketing 跟上機器人變強的潮流?"
date: 2018-05-09
author: "Lin Yen-Cheng"
catalog: true
tags:
  - JavaScript
  - Thinking
  - Marketing
---

### 故事回到 2008 年

Google 在 2008 年推出了 Andriod 的第一個版本，三年後的 2011 年大家普遍開始理解什麼是智慧型手機，那個薑餅人 andriod 2.3.3 Gingerbread，htc 也從那時候開始逐漸邁向巔峰，當大部分人還在用功能型手機時，還記得大學同學買到潮潮 desire 就立馬獻寶???xddd 由此可見當時智慧型手機在大家心中的地位，以及大家對智慧型手機的期待~

<img src="http://cdn.redmondpie.com/wp-content/uploads/2011/08/HTC-Desire-and-the-Problem-with-Gingerbread.jpg">

我也在推甄上研究所的時候，用爸爸公司的購物金買了一隻 LG P350 算是一個低階的入門款，也用他開始我的 Andriod 開發之旅，從那時候開始很多方便的服務開始出現，像是我們開始出現了通訊軟體，我們擁有了能夠定位的網路地圖，漸漸我們漸漸不再用 MSN 和 BBS，漸漸我們不再使用紙本地圖，網路也從 2G -> 3g 到了現在 4G，幾乎每個人都擁有能夠上網的手機了，APP 數量增加的速度也遠超過大家的想像，大量的改變了我們的生活，譬如說：

> 早上起來的第一件事情，關掉手機的鬧鐘，接著打開 facebook 看一下，然後是確認行事曆，啊~下雨了，今天跟朋友約好的地方離捷運站好遠，用個叫車 APP 吧？早餐不知道吃什麼才
> 好，用 google 地圖查查附近有什麼好吃的早餐吧？欸！好像是 IG 熱門打卡景點，就決定是你惹~

就這樣 APP 進入了一個爆炸進步的黃金年代...

### 故事來到 2018 年

如果說從 2010 年開始的 10 年是 app 的黃金年代，也許 2020 年開始的 10 年會是聊天機器人的黃金年代，今年是 2018 年，google io 在幾小時前 demo 了 [AI 機器人][aibot] 目前可以做到的事情，不看還好，一看了突然覺得，靠～真的假的...

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Google Assistant will be able to make actual phone calls for you <a href="https://t.co/AkQjVynER1">pic.twitter.com/AkQjVynER1</a></p>&mdash; The Verge (@verge) <a href="https://twitter.com/verge/status/993946119607549952?ref_src=twsrc%5Etfw">May 8, 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

除了把世界棋王電到不要不要以外，我們可以竟然讓機器人幫我們撥電話去預定一些事情了??!!!! 這其中有一些爆點：

- 可以理解我們心中期待
- 可以知道我們想要什麼
- 可以幫助我們完成目的

那回到我們日常生活中，我們又可以做些什麼來跟上潮流呢？不會 AI 也沒關係，畢竟沒看過豬走路也吃過豬肉，常用的通訊軟體之中，好像就藏了一些小祕密，像是 Line 或是 Facebook 其實都提供了聊天機器人的服務，那又可以做到什麼呢？先看個趨勢，從 Chatbot Marketing 這個關鍵字去查可以看到[一篇文章][chatbot-marketing]，重點如下：

- 28.2 trillion mobile messages are expected to be sent in 2017. This number is about double the amount of messages sent in 2012.
- The open rate for mobile messages is a near perfect 98 percent, which is exponentially higher than the 22 percent rate emails employ.
- Six of the top apps used globally are messaging apps.
- Retention rate of messaging apps is about double the amount of other types of apps.

看不懂英文沒關係，翻譯年糕：

- 推測去年大概傳輸了 28.2 兆則訊息，約是 2012 年的二倍
- 通訊軟體的已讀率 98% >>> Email 的 22%
- 熱門 apps 中前 6 是通訊軟體
- 通訊 app 有 2 倍的可能會被留下來

目前 facebook 和 line 兩家目前都有提供 API，當整合有限狀態機進去時，會有什麼樣的變化? 也許我們就能把分散功能整合在一起，像是透過 messenger 預定航班， 收到航班預定確認時，就馬上透過過去按讚的習慣來推薦旅館景點和交通方式等等資訊，以往這些功能都分散在不同的 app 中，現在彷彿出現了新的可能，對於這種目標指向且有程序性的問題，首先範圍已經很限縮，並不像閒聊會有奇怪的斷句，所以只要做到特定限制下，讓使用者得到想要的資訊我覺得並不困難？也就是有限狀態機的概念，就只是在不同的狀態間移動~

所以我們就可以做到在通訊軟體這個出奇蛋中，一次完成三個願望(誤)

### 我也想要怒寫一個機器人

機器人後端的實作可能為串接 skyplaner 的 API 接著顯示相關結果，透過金流系統串接直接下定，如果機器人本身就是相關信箱服務提供者，像是 gmail 在收到信時可以搭配 google map 自動推薦旅館及遊程和當地交通方式，如果需要收集消費者相關資料並自行推播相關產品的話，可能需要寫一個 backend service 來串接整理相關資訊，大致上就是我們收到 facebook || line 傳來的 user, id 及訊息，接著透過訊息來撈取相關資訊進行回覆，同時也偷存下這個使用者的相關資訊以便未來分析，甚至也可以考慮直接使用 facebook 本身提供的 [messenger 廣告服務][fbad]?

### 不會寫 code 也可以?

也許大家以為實作出一個聊天機器人會需要很多背景知識，但已經有一些技術宅幫大家克服難關了像是 Chatfuel 或是 Manychat 這樣的服務，使用起來都是號稱十分鐘內就可以做出來了!!! 試了一下真的超簡單，歡迎各位大大跟[我的粉專][我的粉專]聊聊天喔 xddd 心動了嗎? 快去試玩吧！xddd

[aibot]: https://ai.googleblog.com/2018/05/duplex-ai-system-for-natural-conversation.html
[chatbot-marketing]: https://www.scripted.com/content-marketing/chatbot-marketing
[我的粉專]: https://www.facebook.com/Im-彥成-219138595338413
[fbad]: https://www.facebook.com/business/products/messenger-for-business
