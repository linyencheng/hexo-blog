---
layout: "about"
title: "關於"
date: 2019-08-20 08:21:33
description: "嗨, 我是彥成，一個前端攻城獅"
header-img: "img/about-bg.jpg"
comments: true
---

## 嗨, 我是彥成，一個前端攻城獅

> 想成為能在世界各地工作的人，選擇投入高移動性的網路產業
> 想用合理的工時工作，認為讀書和工作只是幫助我們更接近喜歡的自己
> 想成為一個對社會有好的影響的人，因為「聰明是天賦，善良是一種選擇」

我畢業於成功大學工程科學系，工科領域涵蓋電機、機械、資訊並期待我們能夠成為跨領域的人才，但實際上卻是在培養考古學家，大學四年因為考試考到懷疑人生，刺激了更多在課業上的反思，後來認為在綜合型大學是個適合多嘗試的環境，所以除了外系課程外也提早體驗碩士班，最後修了 176 個學分才畢業。

### 各種嘗試

> 曾經想要當導遊，在三年的導覽志工服務結束後，發現長期下來並不那麼有趣。
> 曾經想要當老師，當了志工老師也通過教程門檻，發現工科系不太適合當體制內的老師。 
> 曾經不想讀書，因為被培養成考試機器真的很無聊，開始認真參加社團，後來也大量嘗試外系課程。 
> 曾經不想升學，決定體驗做專題、先修研究所的課、參加生涯教練計畫和娃哈哈機電所的暑期實習。
> 曾經不想當血汗工程師，但還是從擅長的事情出發，期待能找到喜歡、擅長、也能支撐生活的工作。

碩士班幸運的直升工科所資訊組，跟著學長學會了嵌入式、和學弟一起開發了利用影像辨識的室內導航 App，後來因為看好網頁發展趨勢，論文實作一個群眾外包概念的藥物服用知識系統原型網站，並透過 UTAUT 去探討成大醫院的病人或家屬對於這個系統的想法，此外也學會了研究方法、寫了全英文的碩士論文。

### 關於職涯

在第一份工作過程中，發現短時間很難追上十年 Java 經驗的主管，而前端框架像是 Vue 和 React 正開始熱門，認為這不但是個趨勢外，更是個同步起跑點的機會，所以工作之餘開始練習並在合適機會建議公司導入。但發展和導入程度仍舊受限 GIS 領域和未來可維護性，於是才決定轉向較容易採用新技術的新創公司。

也在嘗試過後，認為影響社會並不一定要在教育體制內，體制外也能透過各種方式實現，所以先後加入了建置無障礙資訊的社會企業、希望發展線上教育的新創公司，工作內容以 React 和 Node.js 為主，專案類型大多則是從無到有去實作 MVP。

接下來希望有機會能接觸更完整的專案並精進單元測試、行銷相關的能力。此外我認為在新創可以快速接觸到系統全貌，工程師的能力可以很直接影響到專案好壞，這個部分也蠻有成就感的。

### 相關專案

> [咖啡地圖][coffee map]：串接[開源資料][cafenomad] (React)
> [高鐵班次][thsr]：串接[公開資料](https://ptx.transportdata.tw/MOTC/Swagger/#/THSRApi) (React)
> [對戰紀錄表][pokemon competition]：串接 Google Sheet (React)
> [單頁筆記][vue js note]：Vue.js SPA (Vue)

<!-- ### 職場經歷
##### 良師塾事業有限公司, 前端工程師, 2018/1 ~ 至今
> 1. 負責整個[線上教室][Online classroom]、[智慧題庫][Wisdom Bank]前台 (React)
> 2. 開發線上教室討論區後端、即時答題系統 (Socket.io)
> 3. [良師塾人文食飲][Literati Cafe]站台優化 (WordPress)
> 4. 良師塾人文食飲[人力招募][Hiring]、[排班系統][Cafe Shift]的功能開發及維護 (React)
> 5. [良師塾抽兌獎系統][Lucky Draw] (React)
> 6. 導入 Trello，建立 CI/CD 環境與測試 (Jenkins)

##### 众社企股份有限公司, 前端工程師, 2017/4 ~ 2018/1
> 1. 開發[友善旅館登錄系統][Friendly Hotel Registration System]，方便非資訊人員進行相關資料維護 (React)
> 2. 實作[友善旅館][Friendly Hotel]伺服器渲染 SSR、多國語系，改善效能及 SEO (React)
> 3. 開發[新北醫藥通-健康地理資訊查詢系統][Medicare] (GIS, Vue)

##### 北宸科技股份有限公司, 軟體工程師, 2015/9 ~ 2017/10
> 1. 開發圖資更新網站：包含會員、購買、下載、多國語系 (Vue)
[Saipa Website][Saipa Website]、[Kia Website][Kia Website]、[Hyundai Website][Hyundai Website]
> 2. 開發 [Web GIS 前端圖台 SDK][Web GIS SDK] (Openlayers)
> 3. 建構 [Web GIS][Web GIS] 平台，配置系統架構(cache)、部署圖資、API 設計 (Java)
> 4. 開發 Token-Based 會員系統 (Java)
> 5. 4月中~10月中為 part-time 協助專案維護及相關諮詢 -->

[google marketing]: https://goo.gl/wfyrkV
[online classroom]: https://onlineclassroomdaily.liangshishu.com
[lucky draw]: https://luckydraw.liangshishu.com/
[friendly hotel]: https://ourhotel.azurewebsites.net/
[friendly hotel registration system]: https://ourcitylovewebapps.azurewebsites.net/hotelapp/
[web gis sdk]: http://map.polstargps.com/polnavMapAPI/
[web gis]: http://map.polstargps.com/demo/
[saipa website]: http://saipa.polstargps.com/
[kia website]: http://kia.polstargps.com/
[hyundai website]: http://hyundai.polstargps.com
[medicare]: https://www.health.ntpc.gov.tw/medi
[wisdom bank]: https://wisdombank.liangshishu.com/
[literati cafe]: https://literaticafe.liangshishu.com/
[hiring]: https://hiring.liangshishu.com/
[cafe shift]: https://cafeshiftarrangement.liangshishu.com
[thsr]: https://linyencheng.github.io/thsr-app
[vue js note]: https://linyencheng.github.io/vue-note
[coffee map]: https://linyencheng.github.io/coffee-map
[pokemon competition]: https://linyencheng.github.io/pokemon-competition
[cafenomad]: https://cafenomad.tw/developers/docs/v1.2
